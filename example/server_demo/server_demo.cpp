// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include <cassert>
#include <iostream>
#include <exception>
#include <typeinfo>
#include <thread>
#include <chrono>
#include "Server.h"
#include "proto/login.pb.h"


asio::io_service io_service;
net::Server server(io_service);


void handleLogin(net::Packet& packet)
{
    const auto req = (proto::LoginReq*)(packet.GetMessage());
    assert(req != nullptr);

    //
    // do something, e.g auth this user
    //

    proto::LoginRes res;
    res.set_status(0); // 0 means OK
    res.set_server_id("12345");
    res.set_server_name("awesome_server_1");
    server.Write(packet.Session(), SM_LOGIN_STATUS, &res);
}

int main(int argc, char* argv[])
{
	if (argc < 3) 
	{
		std::cerr << "Usage: server_demo [host] [port]" << std::endl;
		return 1;
	}
    
    server.RegMsgCallback(CM_LOGIN, handleLogin);
    server.Start(argv[1], atoi(argv[2]));
    std::cout << "Server started " << argv[1] << ":" << argv[2] << std::endl;

    try
    {
        while (true)
        {
            io_service.poll();
            server.Run();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
    catch (std::exception& ex)
    {
        std::cerr << typeid(ex).name() << ": " << ex.what() << std::endl;
        return 1;
    }

    return 0;
}