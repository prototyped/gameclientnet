// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include <iostream>
#include <exception>
#include <typeinfo>
#include <thread>
#include <chrono>
#include <asio/io_context.hpp>
#include "Net/CodecV1.h"
#include "Net/Client.h"
#include "Net/IPAddress.h"
#include "Net/MsgType.h"
#include "proto/login.pb.h"

using namespace std;

asio::io_context io_service;
net::Endpoint client(io_service);



void handleLoginResult(net::PacketPtr packet)
{
    const auto res = packet->ParseMessage<proto::UserAuthenticateAck>();
    assert(res != nullptr);

    // login result
}

void startLogin()
{
    proto::UserAuthenticateReq req;
    req.set_token("unIJli7fR7nLQExqgVlUyLUTIyxJolNpHHxA6SqR8CEBMJbC");
    client.Send(SERVICE_LOGIN, MsgType::CM_LOGIN, &req);
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        // host format: 127.0.0.1:12345
        std::cerr << "Usage: client_demo [host]" << std::endl;
        return 1;
    }

	std::string address = argv[1]; // "www.example.com:10023"
	std::error_code ec = client.Establish(io_service, address, new net::CodecV1);
	if (ec) 
	{
		cout << ec.message() << endl;
		return 1;
	}
    client.RegisterMessageCallback(MsgType::SM_LOGIN_STATUS, handleLoginResult);
    client.Start(true);

    // send login request
    startLogin();

    try
    {
        while (true)
        {
            io_service.poll();
            client.Run();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
    catch (std::exception& ex)
    {
        std::cerr << typeid(ex).name() << ": " << ex.what() << std::endl;
        return 1;
    }

    return 0;
}