// Copyright 2018-preset Beyondtech Inc. All Rights Reserved.
//
// http://www.3bodygame.com
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#include "echo_server.h"
#include "Utility/Conv.h"
#include "Net/HTTP/Uri.h"
#include "Net/HTTP/HttpStatus.h"

using namespace std::placeholders;


//////////////////////////////////////////////////////////////////////////

HttpEchoServer::~HttpEchoServer()
{
}


int HttpEchoServer::Init()
{

    std::string url = "http://127.0.0.1:9002/echo"; 
    std::string lurl = args_["url"];
    if (lurl != "")
    {
        url = lurl;
    }
    
    net::Uri uri(url);
    prefix_url_ = url;

    int r = server_.Init(uri.host(), to<std::string>(uri.port()));
    if (r != 0)
    {
        return r;
    }

    server_.HandleFunc(uri.path() + "/echo", std::bind(&HttpEchoServer::HandleEcho, this, _1, _2));
    return 0;
}

int HttpEchoServer::Run()
{
    return server_.Run();
}

void HttpEchoServer::HandleEcho(net::IHttpRequest* req, net::IHttpResponseWriter* writer)
{
	const std::string& body = req->GetBody();
	writer->WriteStatus(net::HTTP_OK);
	writer->Write(body);
    
}
