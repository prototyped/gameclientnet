﻿// Copyright © 2018-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#include "echo_server.h"

int main(int argc, char* argv[])
{
	std::map<std::string, std::string> args;
	if (argc > 1) {
		args["url"] = argv[1];
	}
	HttpEchoServer server(args);
	server.Init();
	server.Run();

	return 0;
}
