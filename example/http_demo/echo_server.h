﻿// Copyright © 2018-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include <map>
#include <string>
#include "Net/HTTP/HttpServer.h"


class HttpEchoServer
{
public:
    explicit HttpEchoServer(const std::map<std::string, std::string>& args)
		: args_(args)
    {
    }

    ~HttpEchoServer();

    int Init();
    int Run();

private:
    void HandleEcho(net::IHttpRequest* req, net::IHttpResponseWriter* writer);

private:
	std::map<std::string, std::string>   args_;
    std::string     prefix_url_;
    net::HttpServer server_;
};