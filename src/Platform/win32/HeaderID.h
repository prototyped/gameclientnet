﻿// Copyright © 2018-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include <http.h>

// common HTTP header ID
#define GEN_HTTP_HEADER_ID_MAP(XX) \
    XX("Accept", HttpHeaderAccept) \
    XX("Accept-Charset", HttpHeaderAcceptCharset) \
    XX("Accept-Encoding", HttpHeaderAcceptEncoding) \
    XX("Authorization", HttpHeaderAuthorization) \
    XX("Cache-Control", HttpHeaderCacheControl) \
    XX("Connection", HttpHeaderConnection) \
    XX("Content-Length", HttpHeaderContentLength) \
    XX("Content-Type", HttpHeaderContentType) \
    XX("Content-Encoding", HttpHeaderContentEncoding) \
    XX("Content-Language", HttpHeaderContentLanguage) \
    XX("Content-MD5", HttpHeaderContentMd5) \
    XX("Content-Range", HttpHeaderContentRange) \
    XX("Cookie", HttpHeaderCookie) \
    XX("Expect", HttpHeaderExpect) \
    XX("Expires", HttpHeaderExpires) \
    XX("From", HttpHeaderFrom) \
    XX("Host", HttpHeaderHost) \
    XX("If-Match", HttpHeaderIfMatch) \
    XX("If-Modified-Since", HttpHeaderIfModifiedSince) \
    XX("If-None-Match", HttpHeaderIfNoneMatch) \
    XX("If-Range", HttpHeaderIfRange) \
    XX("If-Unmodified-Since", HttpHeaderIfUnmodifiedSince) \
    XX("Pragma", HttpHeaderPragma) \
    XX("Proxy-Authorization", HttpHeaderProxyAuthorization) \
    XX("Range", HttpHeaderRange) \
    XX("Trailer", HttpHeaderTrailer) \
    XX("Referer", HttpHeaderReferer) \
    XX("Last-Modified", HttpHeaderLastModified) \
    XX("User-Agent", HttpHeaderUserAgent) \
    XX("Upgrade", HttpHeaderUpgrade) \
    XX("Transfer-Encoding", HttpHeaderTransferEncoding) \
    XX("Via", HttpHeaderVia)
    
