﻿// Copyright © 2018-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#include "Platform/PlatformHelper.h"

#if defined(_WIN32)

#include <Windows.h>
#include <wininet.h>
#include <algorithm>
#include <vector>
#include <map>
#include "Utility/ScopeGuard.h"
#include "Utility/StringUtil.h"
#include "Net/HTTP/Uri.h"

#pragma comment(lib, "Wininet.lib")

#define BUFFER_SIZE 4096


namespace {

static HINTERNET hInternet = NULL;

}


struct WinHttpRequestContext
{
    std::string     strUrl;
    std::string     strHost;
    INTERNET_PORT   nPort = 0;
    std::string     strUriPath;
    std::string     strPostData;
    std::map<std::string, std::string> headers;

    DWORD                       dwStatusCode = 0;
    std::vector<std::string>    replyHeaders;
    std::string                 strContent;
    std::string                 strError;

    DWORD SetLastError(DWORD dwErr)
    {
        strError = PlatformHelper::GBKToUtf8(PlatformHelper::GetErrorMessage(dwErr));
        if (strError.empty()) 
        {   
            strError = to<std::string>(dwErr);
        } 
        return dwErr;
    }

    void ParseUri(const std::string& url)
    {
        strUrl = url;
        net::Uri uri(url);
        INTERNET_PORT port = 80;
        if (uri.port() > 0)
        {
            port = uri.port();
        }

        std::string uriPath = uri.path();
        if (!uri.query().empty())
        {
            uriPath += "?";
            uriPath += uri.query();
        }

        nPort = port;
        strHost = uri.host();
        strUriPath = uriPath;
    }
};

std::string PlatformHelper::GetErrorMessage(unsigned int dwError)
{
    // this should be thread local
    char szText[1024] = {};
    DWORD dwRet = FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, NULL, dwError, 0, szText, 1024, NULL);
    szText[dwRet] = '\0';
    return std::string(szText, dwRet);
}

std::wstring PlatformHelper::MultibyteToWide(const std::string& strMultibyte, unsigned int codePage /*= CP_ACP*/)
{
    std::wstring strWide;
    int count = MultiByteToWideChar(codePage, 0, strMultibyte.data(), (int)strMultibyte.length(), NULL, 0);
    if (count > 0)
    {
        strWide.resize(count);
        MultiByteToWideChar(codePage, 0, strMultibyte.data(), (int)strMultibyte.length(),
            const_cast<wchar_t*>(strWide.data()), (int)strWide.length());
    }
    return strWide;
}

std::string PlatformHelper::WideToMultibyte(const std::wstring& strWide, unsigned int codePage /*= CP_ACP*/)
{
    std::string strMultibyte;
    int count = WideCharToMultiByte(codePage, 0, strWide.data(), (int)strWide.length(), NULL, 0, NULL, NULL);
    if (count > 0)
    {
        strMultibyte.resize(count);
        WideCharToMultiByte(codePage, 0, strWide.data(), (int)strWide.length(),
            const_cast<char*>(strMultibyte.data()), (int)strMultibyte.length(), NULL, NULL);
    }
    return strMultibyte;
}


const size_t MAX_OUTPUT_LEN = 4032;

void PlatformHelper::OutputStringToDebugger(const std::string& message)
{
    const std::wstring& text = MultibyteToWide(message, CP_UTF8);
    if (text.size() < MAX_OUTPUT_LEN) //common case
    {
        OutputDebugStringW(text.c_str());
        return;
    }
    size_t outputed = 0;
    while (outputed < text.size())
    {
        // maximum length accepted
        // see http://www.unixwiz.net/techtips/outputdebugstring.html
        wchar_t buf[MAX_OUTPUT_LEN] = {};
        size_t left = text.size() - outputed;
        wcsncpy(buf, text.c_str() + outputed, std::min(left, MAX_OUTPUT_LEN - 1));
        OutputDebugStringW(buf);
        if (left >= MAX_OUTPUT_LEN - 1)
        {
            outputed += MAX_OUTPUT_LEN - 1;
        }
        else
        {
            outputed += left;
        }
    }
}


void PlatformHelper::MustInitializeWinInet()
{
    DWORD dwErr = InternetAttemptConnect(0);
    CHECK(dwErr == ERROR_SUCCESS) << GBKToUtf8(GetErrorMessage(dwErr));

    if (hInternet != NULL)
    {
        InternetCloseHandle(hInternet);
    }
    hInternet = InternetOpenA(NULL, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
    CHECK(hInternet != NULL) << GBKToUtf8(GetErrorMessage(GetLastError()));
}

void PlatformHelper::UnInitializeWinInet()
{
    if (hInternet != NULL)
    {
        InternetCloseHandle(hInternet);
        hInternet = NULL;
    }
}

static int WinHttpDoRequest(const std::string& method, WinHttpRequestContext* ctx)
{
    ctx->headers["User-Agent"] = "WinInet";
    HINTERNET hConnection = InternetConnectA(hInternet, ctx->strHost.data(), ctx->nPort, 
        NULL, NULL, INTERNET_SERVICE_HTTP, 0, 0);
    if (hConnection == NULL)
    {
        return ctx->SetLastError(GetLastError());
    }
    
    SCOPE_EXIT{ InternetCloseHandle(hConnection); };
    
    HINTERNET hFile = HttpOpenRequestA(hConnection, method.data(), ctx->strUriPath.data(), HTTP_VERSIONA,
        NULL, NULL, INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_RELOAD, 0);
    if (hFile == NULL)
    {
        return ctx->SetLastError(GetLastError());
    }

    SCOPE_EXIT{ InternetCloseHandle(hFile); };

    std::string strHeaders;
    for (auto item : ctx->headers)
    {
        std::string line = stringPrintf("%s: %s\r\n", item.first.c_str(), item.second.c_str());
        strHeaders.append(line);
    }
    BOOL bOK = HttpSendRequestA(hFile, strHeaders.data(), strHeaders.size(), (LPVOID)ctx->strPostData.data(), ctx->strPostData.length());
    if (!bOK)
    {
        return ctx->SetLastError(GetLastError());
    }

    char buffer[BUFFER_SIZE] = {};
    DWORD dwLen = BUFFER_SIZE;
    
    bOK = HttpQueryInfoA(hFile, HTTP_QUERY_STATUS_CODE, buffer, &dwLen, NULL);
    if (!bOK)
    {
        return ctx->SetLastError(GetLastError());
    }

    ctx->dwStatusCode = to<uint32_t>(buffer);

    dwLen = BUFFER_SIZE;
    bOK = HttpQueryInfoA(hFile, HTTP_QUERY_RAW_HEADERS_CRLF, buffer, &dwLen, NULL);
    if (!bOK)
    {
        return ctx->SetLastError(GetLastError());
    }

    StringPiece content(buffer, dwLen);
    auto headerFields = Split(content, "\r\n");
    for (auto field : headerFields)
    {
        field = trimWhitespace(field);
        if (!field.empty())
        {
            const std::string& text = field.str();
            ctx->replyHeaders.push_back(text);
            LOG(INFO) << text;
        }
    }

    while (true)
    {
        dwLen = 0;
        bOK = InternetReadFile(hFile, buffer, BUFFER_SIZE, &dwLen);
        if (!bOK)
        {
            return ctx->SetLastError(GetLastError());
        }
        if (dwLen > 0)
        {
            ctx->strContent.append(buffer, dwLen);
            continue;
        }
        break;
    }
    if (ctx->dwStatusCode != 200)
    {
        ctx->strError = stringPrintf("HTTP %d", (int)ctx->dwStatusCode);
    }
    return 0;
}

int PlatformHelper::HttpRequestGet(const std::string& strUrl, std::string& strData, std::string& strErr)
{
    WinHttpRequestContext ctx;
    ctx.ParseUri(strUrl);
    WinHttpDoRequest("GET", &ctx);
    strData.swap(ctx.strContent);
    strErr.swap(ctx.strError);
    return 0;
}
    
int PlatformHelper::HttpRequestPost(const std::string& strUrl, const std::string& strPostdata, std::string& strData, std::string& strErr)
{
    WinHttpRequestContext ctx;
    ctx.strPostData = strPostdata;
    ctx.ParseUri(strUrl);
    ctx.headers["Content-Type"] = "application/octet-stream";
    WinHttpDoRequest("POST", &ctx);
    strData.swap(ctx.strContent);
    strErr.swap(ctx.strError);
    return 0;
}

#endif // _WIN32
