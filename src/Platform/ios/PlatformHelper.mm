#include "Platform/PlatformHelper.h"
#import <Foundation/Foundation.h>
#include <sys/stat.h>
#include <dirent.h>
#include <iostream>
#include <fstream>

// send HTTP request to `strUrl`, and retrieve reponse data to `strData`
int CPlatformHelper::HttpRequestGet(const std::string& strUrl, std::string& strData, std::string& strErr)
{
    NSString *urlString= [NSString stringWithCString:strUrl.c_str() encoding:[NSString defaultCStringEncoding]];
    NSLog(@"HttpRequestGet start %@", urlString);
    //NSString *urlString = [NSString stringWithString:strUrl];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    //[request setHTTPMethod:@"GET"];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSLog(@"HttpRequestGet: Firing synchronous url connection...");
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if ([data length] > 0 && error == nil) {
        NSLog(@"HttpRequestGet: %lu bytes of data was returned.",(unsigned long)[data length]);
        NSLog(@"HttpRequestGet: %@", data);
        const void* pdata = [data bytes];
        strData.assign((const char*)pdata, data.length);
    }else if([data length] == 0 && error == nil){
        NSLog(@"HttpRequestGet: No data was return.");
        strErr = "no data";
    }else if (error != nil){
        NSLog(@"HttpRequestGet: Error happened = %@", error);
        strErr = [[error localizedDescription] UTF8String];
    }
    NSLog(@"HttpRequestGet done.");
    return 0;
}

// send HTTP post request to `strUrl` with `strPostdata`, and retrieve reponse data to `strData`
int CPlatformHelper::HttpRequestPost(const std::string& strUrl, const std::string& strPostdata, std::string& strData, std::string& strErr)
{
    NSString *urlString= [NSString stringWithCString:strUrl.c_str() encoding:[NSString defaultCStringEncoding]];
    NSLog(@"HttpRequestPost start %@", urlString);
    NSData *postData = [NSData dataWithBytes:strPostdata.c_str() length:strPostdata.length()];
    NSString *postLength = [NSString stringWithFormat:@"%ld", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if ([data length] > 0 && error == nil) {
        NSLog(@"HttpRequestPost： %lu bytes of data was returned.",(unsigned long)[data length]);
        NSLog(@"%@", data);
        const void* pdata = [data bytes];
        strData.assign((const char*)pdata, data.length);
    }else if([data length] == 0 && error == nil){
        NSLog(@"HttpRequestPost： No data was return.");
        strErr = "no data";
    }else if (error != nil){
        NSLog(@"Error happened = %@", error);
        strErr = [[error localizedDescription] UTF8String];
    }
    NSLog(@"HttpRequestPost done.");
    return 0;
}
