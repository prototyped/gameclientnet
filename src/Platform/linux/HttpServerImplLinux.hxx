// Copyright 2015-2018 Beyondtech, Inc. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form 
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially 
// exploit the content. Nor may you transmit it or store it in any other website or 
// other form of electronic retrieval system.

#include "HttpServer.h"
#include <stdint.h>
#include <crow.h>
#include "Utility/Conv.h"


using namespace std::placeholders;

namespace net {

// HTTP request object
class HttpRequest : public IHttpRequest
{
public:
    explicit HttpRequest(const crow::request& request)
        : request_(request)
    {}

    const std::string& GetURL()
    {
        return request_.url;
    }

    std::string GetHeader(const std::string& key)
    {
        //return request_.headers[key];
        return "";
    }

    const std::string& GetBody()
    {
        return request_.body;
    }

private:
    const crow::request& request_;
};

// response writer
class HttpResponseWriter : public IHttpResponseWriter
{
public:
    explicit HttpResponseWriter(crow::response& response)
        : response_(response)
    {}

    int WriteHeader(const std::string& key, const std::string& value)
    {
        response_.add_header(key, value);
        return 0;
    }

    int Write(const std::string& data)
    {
        response_.write(data);
        return 0;
    }

    int WriteStatus(int status)
    {
        response_.code = status;
        response_.end();
        return 0;
    }
private:
    crow::response& response_;
};

//////////////////////////////////////////////////////////////////////////

// HTTP server implementation
class HttpServer::HttpServerImpl
{
public:
    HttpServerImpl();
    ~HttpServerImpl();

    std::string host;
    uint16_t port;
    crow::SimpleApp app;

    void HandlerAdapter(const crow::request& request, crow::response& response, HttpHandleFunc fn)
    {
        HttpRequest req(request);
        HttpResponseWriter writer(response);
        fn(&req, &writer);
    }
};



//////////////////////////////////////////////////////////////////////////

HttpServer::HttpServer()
{
}

HttpServer::~HttpServer()
{
}

int HttpServer::Init(const std::string& host, const std::string& port)
{
    impl_.reset(new HttpServerImpl);
    impl_->host = host;
    impl_->port = to<uint16_t>(port);
    return 0;
}

int HttpServer::Run()
{
    crow::logger::setLogLevel(crow::LogLevel::Debug);
    impl_->app.port(impl_->port);
    impl_->app.bindaddr(impl_->host);
    impl_->app.run(); // single thread
    return 0;
}

// register both GET and POST method
void HttpServer::HandleFunc(std::string pattern, HttpHandleFunc fn)
{
    auto lambda = std::bind(&HttpServer::HttpServerImpl::HandlerAdapter, impl_.get(), _1, _2, fn);
    impl_->app.route_dynamic(std::move(pattern)).methods(crow::HTTPMethod::Post)(lambda);
    impl_->app.route_dynamic(std::move(pattern)).methods(crow::HTTPMethod::Get)(lambda);
}

} // namespace net
