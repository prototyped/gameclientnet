﻿#pragma once

#include <stdint.h>
// Copyright © 2018-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#include <string>


class PlatformHelper
{
public:
    // Get string description of an error code
	static std::string GetErrorMessage(unsigned int dwError);
    
    // Encode text between multibyte (both GBK and UTF-8) and wide characters (UCS-2, a subset of UTF-16)
	static std::wstring MultibyteToWide(const std::string& strMultibyte, unsigned int codePage = 0);
	static std::string WideToMultibyte(const std::wstring& strWide, unsigned int codePage = 0);
    
    inline static std::string GBKToUtf8(const std::string& strText)
    {
        return WideToMultibyte(MultibyteToWide(strText), 65001); // CP_UTF8
    }
    
	static void OutputStringToDebugger(const std::string& message);
    
    static void MustInitializeWinInet();
	static void UnInitializeWinInet();


    // HTTP request GET
    static int HttpRequestGet(const std::string& strUrl, std::string& strData, std::string& strErr);
    
    // HTTP request POST
    static int HttpRequestPost(const std::string& strUrl, const std::string& strPostdata, std::string& strData, std::string& strErr);
};
