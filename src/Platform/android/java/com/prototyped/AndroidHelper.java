package com.prototyped;

import java.io.*;
import java.util.*;
import android.os.*;
import android.provider.Settings;
import okhttp3.*;


public class BeyondEngineHelper
{
    private static Activity _activity;
    private static String storageRootDirectory = "";
    
    public static SetActivity(Activity activity) {
        _activity = activity;
    }
    
    // Get an unique ID of this device
    // https://developer.android.com/training/articles/user-data-ids#java
    public static String GetUniqueDeviceID() {
        String uniqueText = GetOrCreateFileUUID();
        if (uniqueText == "") {
            String serial = android.os.Build.SERIAL;
            if (serial != null) {
                uniqueText += serial;
            }
            String androidID = Settings.Secure.getString(_activity.getContentResolver(), Settings.Secure.ANDROID_ID);
            if (androidID != null) {
                uniqueText += androidID;
            }
            String cpuSerial = GetCPUSerial();
            if (cpuSerial != "") {
                uniqueText += cpuSerial;
            }
        }
        return uniqueText;
    }
    
    public static String GetCPUSerial() {
        String serial = "";
        try {
            File file = new File("/proc/cpuinfo");
            if (file.exists()) {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                while (true) {
                    String line = reader.readLine();
                    if (line == null) {
                        break;
                    }
                    int index = line.indexOf("Serial");
                    if (index >= 0) {
                        serial = line.substring(index + 1).trim();
                        Log.i("cpu serial", serial);
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("BeyondEngine", ex.toString());
        }
        return serial;
    }
    
    public static String GetOrCreateFileUUID() {
        String uniqueString = "";
        String filepath = GetExternalDirectoryPath() + "/guid";
        try {
            File file = new File(filepath);
            if (!file.exists()) {
                if (file.createNewFile()) {
                    uniqueString = UUID.randomUUID().toString();
                    OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file));
                    writer.write(uniqueString);
                    writer.close();
                } else {
                    Log.e("BeyondEngine", "cannot create uuid file " + filepath);
                }
            } else {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String line = reader.readLine();
                if (line != null) {
                    uniqueString = line.trim();
                }
            }
        } catch (Exception ex) {
            Log.e("BeyondEngine", String.format("cannot create uuid file %s, %s", filepath, ex.toString()));
        }
        return uniqueString;
    }
    
    public static String GetExternalDirectoryPath()
    {
        if (storageRootDirectory.isEmpty()) {
            initStorageDirectory();
        }
        return storageRootDirectory;
    }
    
    public static boolean initStorageDirectory() {
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return false;
        }
        String rootDir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + _activity.getPackageName();
        File dir = new File(rootDir);
        if (dir.exists()) {
            storageRootDirectory = rootDir;
            return true;
        } else {
            if (dir.mkdirs()) {
                return true;
            } else {
                Log.e("xnet", "cannot create storage dir " + rootDir);
            }
        }

        rootDir = "/mnt/sdcard/" + _activity.getPackageName();
        dir = new File(rootDir);
        if (dir.exists()) {
            return true;
        } else {
            if (dir.mkdirs()) {
                storageRootDirectory = rootDir;
                return true;
            } else {
                Log.e("BeyondEngine", "cannot create storage dir " + rootDir);
            }
        }
        return false;
    }
    
    // UTF-16 string to UTF-8 bytes
    static byte[] makeByteArrayResponse(String errText, byte[] content) {
        errText.replaceAll(HTTP_SEP, "");
        if (errText == "")
        {
            return content;
        }
        if (content != null && content.length > 0)
        {
            return content;
        }
        try
        {
            byte[] errbytes = errText.getBytes("UTF-8");
            byte[] sepBytes = HTTP_SEP.getBytes("UTF-8");
            byte[] newcontent = new byte[errbytes.length + sepBytes.length + content.length];
            int pos = 0;
            for (int i = 0; i < errbytes.length; i++)
            {
                newcontent[pos] = errbytes[i];
                pos++;
            }
            for (int i = 0; i < sepBytes.length; i++)
            {
                newcontent[pos] = sepBytes[i];
                pos++;
            }
            for (int i = 0; i < content.length; i++)
            {
                newcontent[pos] = content[i];
                pos++;
            }
            return newcontent;
        }
        catch (Exception ex)
        {
            Log.e("BeyondEngine", ex.toString());
            return content;
        }
    }
    
    public static byte[] HttpGet(String url) {
        //// uncomment lines below to make main UI thread be able to do blocking network operations
        // StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        // StrictMode.setThreadPolicy(policy);

        String errText = "";
        byte[] content = {};
        try {
            Request request = new Request.Builder().url(url).build();
            Response response = httpClient.newCall(request).execute();
            if (response.code() == 200) {
                Log.i("BeyondEngine", response.headers().toString());
                content = response.body().bytes();
            } else {
                errText = response.message();
            }
            response.close();
        } catch (Exception ex) {
            errText = ex.toString();
            Log.e("BeyondEngine", ex.toString());
        }
        return makeByteArrayResponse(errText, content);
    }

 
    public static final MediaType BINARY = MediaType.parse("Content-Type: application/octet-stream");
    
    public static byte[] HttpPost(String url, byte[] data) {
        //// uncomment lines below to make main UI thread be able to do blocking network operations
        // StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        // StrictMode.setThreadPolicy(policy);

        String errText = "";
        byte[] content = {};
        try {
            RequestBody body = RequestBody.create(BINARY, data);
            Request request = new Request.Builder().url(url).post(body).build();
            Response response = httpClient.newCall(request).execute();
            if (response.code() == 200) {
                Log.i("BeyondEngine", response.headers().toString());
                content = response.body().bytes();
            } else {
                errText = response.message();
            }
            response.close();
        } catch (Exception ex) {
            errText = ex.toString();
            Log.e("BeyondEngine", ex.toString());
        }

        return makeByteArrayResponse(errText, content);
    }
}