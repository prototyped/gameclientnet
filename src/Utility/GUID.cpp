// Copyright 2018-preset ichenq@outlook.com. All Rights Reserved.
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#include "GUID.h"
#include "Logging.h"
#include "StringUtil.h"


std::string GenerateGUID()
{
    uint64_t bytes[2] = {};
    RandBytes(&bytes, sizeof(bytes));

    // Set the GUID to version 4 as described in RFC 4122, section 4.4.
    // The format of GUID version 4 must be xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx,
    // where y is one of [8, 9, A, B].

    // Clear the version bits and set the version to 4:
    bytes[0] &= 0xffffffffffff0fffULL;
    bytes[0] |= 0x0000000000004000ULL;

    // Set the two most significant bits (bits 6 and 7) of the
    // clock_seq_hi_and_reserved to zero and one, respectively:
    bytes[1] &= 0x3fffffffffffffffULL;
    bytes[1] |= 0x8000000000000000ULL;
    return stringPrintf("%08x-%04x-%04x-%04x-%012llx",
        static_cast<unsigned int>(bytes[0] >> 32),
        static_cast<unsigned int>((bytes[0] >> 16) & 0x0000ffff),
        static_cast<unsigned int>(bytes[0] & 0x0000ffff),
        static_cast<unsigned int>(bytes[1] >> 48),
        bytes[1] & 0x0000ffffffffffffULL);
}


bool IsValidGUID(const std::string& guid)
{
    const size_t kGUIDLength = 36U;
    if (guid.length() != kGUIDLength)
        return false;

    for (size_t i = 0; i < guid.length(); ++i) {
        char current = guid[i];
        if (i == 8 || i == 13 || i == 18 || i == 23) 
        {
            if (current != '-')
                return false;
        }
        else 
        {
            if (!IsHexDigit(current))
                return false;
        }
    }
    return true;
}