// Copyright 2015-present Beyondtech, Inc. All Rights Reserved.
//
// http://www.3bodygame.com
//
// Any redistribution or reproduction of part or all of the contents in any form 
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially 
// exploit the content. Nor may you transmit it or store it in any other website or 
// other form of electronic retrieval system.

#include "Buffer.h"

BufferPtr Buffer::create()
{
    return std::make_shared<Buffer>();
}
