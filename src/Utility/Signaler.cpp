// Copyright 2018-preset ichenq@outlook.com. All Rights Reserved.
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#include "Signaler.h"
#include "Logging.h"
#include "Portability.h"
#include <chrono>
#include <thread>

#if defined(_WIN32)
#include <winsock2.h>
#else
#include <unistd.h>
#include <netinet/tcp.h>
#include <sys/types.h>
#include <sys/socket.h>
#endif // _WIN32

// Helper to wait on close(), for non-blocking sockets, until it completes
// If EAGAIN is received, will sleep briefly (1-100ms) then try again, until
// the overall timeout is reached.
#if !defined(_WIN32)
static int close_wait_ms(int fd, unsigned int max_ms_ = 2000)
{
    unsigned int ms_so_far = 0;
    unsigned int step_ms = max_ms_ / 10;
    if (step_ms < 1)
        step_ms = 1;
    if (step_ms > 100)
        step_ms = 100;

    int rc = 0; // do not sleep on first attempt
    do {
        if (rc == -1 && errno == EAGAIN)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(step_ms));
            ms_so_far += step_ms;
        }
        rc = close(fd);
    } while (ms_so_far < max_ms_ && rc == -1 && errno == EAGAIN);

    return rc;
}
#endif // !_WIN32

Signaler::Signaler()
{
    //  Create the socketpair for signaling.
    if (make_fdpair(&r_, &w_) == 0)
    {
        unblock_socket(w_);
        unblock_socket(r_);
    }
}

Signaler::~Signaler()
{
#ifdef __linux__
    if (r_ != INVALID_SOCKET)
    {
        int rc = close_wait_ms(r_);
        CHECK(rc == 0);
    }
#elif defined(_WIN32)
    if (w_ != INVALID_SOCKET)
    {
        const struct linger so_linger = { 1, 0 };
        int rc = setsockopt(w_, SOL_SOCKET, SO_LINGER,
            (const char *)&so_linger, sizeof so_linger);
        //  Only check shutdown if WSASTARTUP was previously done
        if (rc == 0 || WSAGetLastError() != WSANOTINITIALISED)
        {
            CHECK(rc != SOCKET_ERROR);
            rc = closesocket(w_);
            CHECK(rc != SOCKET_ERROR);
            if (r_ == INVALID_SOCKET)
                return;
            rc = closesocket(r_);
            CHECK(rc != SOCKET_ERROR);
        }
    }
#else
    if (w_ != INVALID_SOCKET)
    {
        int rc = close_wait_ms(w_);
        CHECK(rc == 0);
    }
    if (r_ != INVALID_SOCKET)
    {
        int rc = close_wait_ms(r_);
        CHECK(rc == 0);
    }
#endif // __linux__
}

SOCKET Signaler::getFd() const
{
    return r_;
}

bool Signaler::IsValid() const
{
    return r_ == INVALID_SOCKET;
}

void Signaler::Send()
{
#if defined(__linux__)
    const uint64_t inc = 1;
    ssize_t sz = write(w_, &inc, sizeof(inc));
    CHECK(sz == sizeof(inc));
#elif defined(_WIN32)
    unsigned char dummy = 0;
    while (true) {
        int nbytes = ::send(w_, (char *)&dummy, sizeof(dummy), 0);
        CHECK(nbytes != SOCKET_ERROR);
        if (UNLIKELY(nbytes == SOCKET_ERROR))
            continue;
        CHECK(nbytes == sizeof(dummy));
        break;
    }
#else
    unsigned char dummy = 0;
    while (true)
    {
        ssize_t nbytes = ::send(w_, &dummy, sizeof(dummy), 0);
        if (UNLIKELY(nbytes == -1 && errno == EINTR))
            continue;
        CHECK(nbytes == sizeof dummy);
        break;
    }
#endif // __linux__
}

int Signaler::Wait(int timeout)
{
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(r_, &fds);
    struct timeval tv;
    if (timeout >= 0)
    {
        tv.tv_sec = timeout / 1000;
        tv.tv_usec = timeout % 1000 * 1000;
    }
#ifdef _WIN32
    int rc = select(0, &fds, NULL, NULL, timeout >= 0 ? &tv : NULL);
    CHECK(rc != SOCKET_ERROR);
#else
    int rc = select(r_ + 1, &fds, NULL, NULL, timeout >= 0 ? &tv : NULL);
    if (UNLIKELY(rc < 0))
    {
        CHECK(errno == EINTR);
        return -1;
    }
#endif // _WIN32
    if (UNLIKELY(rc == 0)) {
        errno = EAGAIN;
        return -1;
    }
    CHECK(rc == 1);
    return 0;
}

void Signaler::Recv()
{
#if defined(__linux__)
    uint64_t dummy;
    ssize_t sz = read(r_, &dummy, sizeof(dummy));
    CHECK(sz == sizeof(dummy));

    //  If we accidentally grabbed the next signal(s) along with the current
    //  one, return it back to the eventfd object.
    if (UNLIKELY(dummy > 1)) {
        const uint64_t inc = dummy - 1;
        ssize_t sz2 = write(w_, &inc, sizeof(inc));
        CHECK(sz2 == sizeof(inc));
        return;
    }

    CHECK(dummy == 1);
#else
    unsigned char dummy;
    int nbytes = ::recv(r_, (char *)&dummy, sizeof(dummy), 0);
#ifdef _WIN32
    CHECK(nbytes != SOCKET_ERROR);
#else
    CHECK(nbytes >= 0);
#endif // _WIN32
    CHECK(nbytes == sizeof(dummy));
    CHECK(dummy == 0);
#endif // __linux__
}
