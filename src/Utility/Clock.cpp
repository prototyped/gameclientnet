// Copyright (C) 2018-present ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "Clock.h"
#include <ctime>
#include <chrono>
#include "Logging.h"
#include "Portability.h"

#if defined(_WIN32)
#include <Windows.h>
#else
#include <time.h>
#include <sys/time.h>
#endif


int64_t Clock::GetNowTickCount()
{
#if defined(_WIN32)
    int64_t freq, ticks;
    BOOL r = QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
    CHECK(r == 0) << GetLastError();
    QueryPerformanceCounter((LARGE_INTEGER*)&ticks);
    return (ticks * 1000000000UL) / freq;
#else
    timespec ts;
    int r = clock_gettime(CLOCK_MONOTONIC, &ts);
    CHECK(r != -1) << errno;
    return (ts.tv_sec * 1000000000UL) + ts.tv_nsec;
#endif
}

int64_t Clock::GetCurrentTimeMillis()
{
    auto now = std::chrono::system_clock::now();
    return std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()).count();
}

std::string Clock::GetCurrentTimeString()
{
    int64_t timepoint = GetCurrentTimeMillis();
    struct tm info = *localtime((const time_t*)&timepoint);
    char buffer[128] = {};
    int n = snprintf(buffer, 128, "%d-%02d-%02d %02d:%02d:%02d.%03d", 1900 + info.tm_year, info.tm_mon + 1,
        info.tm_wday, info.tm_hour, info.tm_min, info.tm_sec, (int)(timepoint % 1000));
    CHECK(n > 0);
    return std::string(buffer, n);
}

