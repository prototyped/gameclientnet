﻿// Copyright © 2016-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include <cstdint>
#include <cstdlib>


// Endianness of the machine.
//
// GCC 4.6 provided macro for detecting endianness of the target machine. But other
// compilers may not have this. User can define XNET_ENDIAN to either
// XNET_LITTLEENDIAN or XNET_BIGENDIAN.
// Default detection implemented with reference to
//  https://gcc.gnu.org/onlinedocs/gcc-4.6.0/cpp/Common-Predefined-Macros.html
//  http://www.boost.org/doc/libs/1_42_0/boost/detail/endian.hpp

#define XNET_LITTLEENDIAN  0   //!< Little endian machine
#define XNET_BIGENDIAN     1   //!< Big endian machine

// Detect with GCC 4.6's macro
#  ifdef __BYTE_ORDER__
#    if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#      define XNET_ENDIAN   XNET_LITTLEENDIAN
#    elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#      define XNET_ENDIAN   XNET_BIGENDIAN
#    else
#      error Unknown machine endianess detected. User needs to define BEYONDENGINE_ENDIAN.
#    endif // __BYTE_ORDER__
// Detect with GLIBC's endian.h
#  elif defined(__GLIBC__)
#    include <endian.h>
#    if (__BYTE_ORDER == __LITTLE_ENDIAN)
#      define XNET_ENDIAN   XNET_LITTLEENDIAN
#    elif (__BYTE_ORDER == __BIG_ENDIAN)
#      define XNET_ENDIAN   XNET_BIGENDIAN
#    else
#      error Unknown machine endianess detected. User needs to define BEYONDENGINE_ENDIAN.
#   endif // __GLIBC__
// Detect with _LITTLE_ENDIAN and _BIG_ENDIAN macro
#  elif defined(_LITTLE_ENDIAN) && !defined(_BIG_ENDIAN)
#    define XNET_ENDIAN     XNET_LITTLEENDIAN
#  elif defined(_BIG_ENDIAN) && !defined(_LITTLE_ENDIAN)
#    define XNET_ENDIAN     XNET_BIGENDIAN
// Detect with architecture macros
#  elif defined(__sparc) || defined(__sparc__) || defined(_POWER) || defined(__powerpc__) || defined(__ppc__) || defined(__hpux) || defined(__hppa) || defined(_MIPSEB) || defined(_POWER) || defined(__s390__)
#    define XNET_ENDIAN     XNET_BIGENDIAN
#  elif defined(__i386__) || defined(__alpha__) || defined(__ia64) || defined(__ia64__) || defined(_M_IX86) || defined(_M_IA64) || defined(_M_ALPHA) || defined(__amd64) || defined(__amd64__) || defined(_M_AMD64) || defined(__x86_64) || defined(__x86_64__) || defined(_M_X64) || defined(__bfin__)
#    define XNET_ENDIAN     XNET_LITTLEENDIAN
#  elif defined(_MSC_VER) && defined(_M_ARM)
#    define XNET_ENDIAN     XNET_LITTLEENDIAN
#  else
#    error Unknown machine endianess detected. User needs to define XNET_ENDIAN.
#  endif


#if defined(_MSC_VER)
#define XNET_SWAP64(x)  _byteswap_uint64((x))
#define XNET_SWAP32(x)  _byteswap_ulong((x))
#define XNET_SWAP16(x)  _byteswap_ushort((x))
#else
#define XNET_SWAP64(x)  __builtin_bswap64((x))
#define XNET_SWAP32(x)  __builtin_bswap32((x))
#define XNET_SWAP16(x)  __builtin_bswap16((x))
#endif

inline int16_t HostToNetwork16(int16_t value)
{
#if XNET_ENDIAN == XNET_BIGENDIAN
    return value;
#else 
    return (int16_t)XNET_SWAP16(value);
#endif
}

inline int32_t HostToNetwork32(int32_t value)
{
#if XNET_ENDIAN == XNET_BIGENDIAN
    return value;
#else 
    return (int32_t)XNET_SWAP32(value);
#endif
}

inline int64_t HostToNetwork64(int64_t value)
{
#if XNET_ENDIAN == XNET_BIGENDIAN
    return value;
#else 
    return (int64_t)XNET_SWAP64(value);
#endif
}

class LittleEndian
{
public:
    static inline uint16_t GetUint16(const void* p)
    {
        const uint8_t* data = (const uint8_t*)p;
        return uint16_t(data[0]) | (uint16_t(data[1]) << 8);
    }

    static inline void PutUint16(void* p, uint16_t val)
    {
        uint8_t* data = (uint8_t*)p;
        data[0] = uint8_t(val & 0xFF);
        data[1] = uint8_t(val >> 8);
    }

    static inline uint32_t GetUint32(const void* p)
    {
        const uint8_t* data = (const uint8_t*)p;
        return uint32_t(data[0]) | (uint32_t(data[1]) << 8)
            | (uint32_t(data[2]) << 16) | (uint32_t(data[3]) << 24);
    }

    static inline void PutUint32(void* p, uint32_t val)
    {
        uint8_t* data = (uint8_t*)p;
        data[0] = uint8_t(val & 0x000000FF);
        data[1] = uint8_t(val >> 8);
        data[2] = uint8_t(val >> 16);
        data[3] = uint8_t(val >> 24);
    }

    static inline uint64_t GetUint64(const void* p)
    {
        const uint8_t* data = (const uint8_t*)p;
        return uint64_t(data[0]) | (uint64_t(data[1]) << 8)
            | (uint64_t(data[2]) << 16) | (uint64_t(data[3]) << 24)
            | (uint64_t(data[4]) << 32) | (uint64_t(data[5]) << 40)
            | (uint64_t(data[6]) << 48) | (uint64_t(data[7]) << 56);
    }

    static inline void PutUint64(void* p, uint64_t val)
    {
        uint8_t* data = (uint8_t*)p;
        data[0] = uint8_t(val & 0x000000FFUL);
        data[1] = uint8_t(val >> 8);
        data[2] = uint8_t(val >> 16);
        data[3] = uint8_t(val >> 24);
        data[4] = uint8_t(val >> 32);
        data[5] = uint8_t(val >> 40);
        data[6] = uint8_t(val >> 48);
        data[7] = uint8_t(val >> 56);
    }
};

class BigEndian
{
public:
    static inline uint16_t GetUint16(const void* p)
    {
        const uint8_t* data = (const uint8_t*)p;
        return (uint16_t(data[1]) << 8) | uint16_t(data[0]);
    }

    static inline void PutUint16(void* p, uint16_t val)
    {
        uint8_t* data = (uint8_t*)p;
        data[0] = uint8_t(val >> 8);
        data[1] = uint8_t(val & 0xFF);
    }

    static inline uint32_t GetUint32(const void* p)
    {
        const uint8_t* data = (const uint8_t*)p;
        return (uint32_t(data[3]) << 24) | (uint32_t(data[2]) << 16)
            | (uint32_t(data[1]) << 8) | uint32_t(data[0]);
    }

    static inline void PutUint32(void* p, uint32_t val)
    {
        uint8_t* data = (uint8_t*)p;
        data[3] = uint8_t(val >> 24);
        data[0] = uint8_t(val >> 24);
        data[1] = uint8_t(val >> 16);
        data[2] = uint8_t(val >> 8);
        data[3] = uint8_t(val & 0x000000FF);
    }

    static inline uint64_t GetUint64(const void* p)
    {
        const uint8_t* data = (const uint8_t*)p;
        return uint64_t(data[7]) | (uint64_t(data[6]) << 8)
            | (uint64_t(data[5]) << 16) | (uint64_t(data[4]) << 24)
            | (uint64_t(data[3]) << 32) | (uint64_t(data[2]) << 40)
            | (uint64_t(data[1]) << 48) | (uint64_t(data[0]) << 56);
    }

    static inline void PutUint64(void* p, uint64_t val)
    {
        uint8_t* data = (uint8_t*)p;
        data[0] = uint8_t(val >> 56);
        data[1] = uint8_t(val >> 48);
        data[2] = uint8_t(val >> 40);
        data[3] = uint8_t(val >> 32);
        data[4] = uint8_t(val >> 24);
        data[5] = uint8_t(val >> 16);
        data[6] = uint8_t(val >> 8);
        data[7] = uint8_t(val & 0x000000FFUL);
    }
};
