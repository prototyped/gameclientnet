// Copyright 2018-preset ichenq@outlook.com. All Rights Reserved.
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include "FileDescriptor.h"

//  This is a cross-platform equivalent to signal_fd. However, as opposed
//  to signal_fd there can be at most one signal in the signaler at any
//  given moment. Attempt to send a signal before receiving the previous
//  one will result in undefined behaviour.
class Signaler
{
public:
    Signaler();
    ~Signaler();

    Signaler(const Signaler&) = delete;
    Signaler& operator=(const Signaler&) = delete;

    // Returns the socket/file descriptor
    // May return retired_fd if the signaler could not be initialized.
    SOCKET getFd() const;

    // send notify
    void Send();

    // wait until been notified
    int Wait(int timeout);

    // recv notify
    void Recv();

    bool IsValid() const;

private:
    //  Underlying write & read file descriptor
    //  Will be -1 if an error occurred during initialization, e.g. we
    //  exceeded the number of available handles
    SOCKET  r_ = INVALID_SOCKET;   // reader fd
    SOCKET  w_ = INVALID_SOCKET;   // writer fd
};
