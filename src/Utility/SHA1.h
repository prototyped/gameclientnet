// Copyright 2018-preset ichenq@outlook.com. All Rights Reserved.
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include <stdint.h>
#include <string>
#include "Range.h"

// see https://github.com/chromium/chromium/blob/master/base/sha1.cc

// Length in bytes of a SHA-1 hash.
static const size_t kSHA1Length = 20;  

// Computes the SHA-1 hash of the input string |str| and returns the full hash.
std::string SHA1HashString(StringPiece str);
std::string SHA1HashHexString(StringPiece str);

// Computes the SHA-1 hash of the |len| bytes in |data| and puts the hash
// in |hash|. |hash| must be kSHA1Length bytes long.
void SHA1HashBytes(const uint8_t* data, size_t len, uint8_t* hash);
