// Copyright 2018-preset ichenq@outlook.com. All Rights Reserved.
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#include "FileDescriptor.h"
#include "ScopeGuard.h"
#include "Logging.h"

#ifdef _WIN32
#include <ws2tcpip.h>
#include <mstcpip.h>
#include <mswsock.h>
#include <iphlpapi.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#endif

#ifdef __linux__
#include <sys/eventfd.h>
#endif

#if defined(__linux__)
static int make_pair_eventfd(int* r, int* w)
{
    int flags = 0;
    //  Setting this option result in sane behaviour when exec() functions
    //  are used. Old sockets are closed and don't block TCP ports, avoid
    //  leaks, etc.
    flags |= EFD_CLOEXEC;
    int fd = eventfd(0, flags);
    if (fd == -1)
    {
        CHECK(errno == ENFILE || errno == EMFILE);
        *w = *r = -1;
        return -1;
    }
    *w = *r = fd;
    return 0;
}
#endif

#ifdef _WIN32

static void setSockOpt(SOCKET fd)
{
    BOOL tcp_nodelay = 1;
    int rc = setsockopt(fd, IPPROTO_TCP, TCP_NODELAY,
        (char *)&tcp_nodelay, sizeof tcp_nodelay);
    CHECK(rc != SOCKET_ERROR);

    int sio_loopback_fastpath = 1;
    DWORD numberOfBytesReturned = 0;

    rc = WSAIoctl(fd, SIO_LOOPBACK_FAST_PATH, &sio_loopback_fastpath,
        sizeof sio_loopback_fastpath, NULL, 0,
        &numberOfBytesReturned, 0, 0);

    if (SOCKET_ERROR == rc)
    {
        DWORD lastError = ::WSAGetLastError();
        CHECK(lastError == WSAEOPNOTSUPP);
    }
}

static int make_pair_winsock(SOCKET* r, SOCKET* w)
{
    //  Windows has no 'socketpair' function. CreatePipe is no good as pipe
    //  handles cannot be polled on. Here we create the socketpair by hand.
    *w = INVALID_SOCKET;
    *r = INVALID_SOCKET;

    //  Create listening socket.
    SOCKET listener = socket(AF_INET, SOCK_STREAM, 0);
    CHECK(listener != INVALID_SOCKET);

    //  Set SO_REUSEADDR and TCP_NODELAY on listening socket.
    BOOL so_reuseaddr = 1;
    int rc = setsockopt(listener, SOL_SOCKET, SO_REUSEADDR,
        (char *)&so_reuseaddr, sizeof so_reuseaddr);
    CHECK(rc != SOCKET_ERROR);

    setSockOpt(listener);

    //  Init sockaddr to signaler port.
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof addr);
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    addr.sin_port = 0;

    //  Create the writer socket.
    *w = socket(AF_INET, SOCK_STREAM, 0);
    CHECK(*w != INVALID_SOCKET);

    //  Set TCP_NODELAY on writer socket.
    setSockOpt(*w);

    //  Bind listening socket to signaler port.
    rc = bind(listener, (const struct sockaddr *) &addr, sizeof addr);
    if (rc != SOCKET_ERROR)
    {
        //  Retrieve ephemeral port number
        int addrlen = sizeof addr;
        rc = getsockname(listener, (struct sockaddr *) &addr, &addrlen);
    }

    //  Listen for incoming connections.
    if (rc != SOCKET_ERROR)
        rc = listen(listener, 1);

    //  Connect writer to the listener.
    if (rc != SOCKET_ERROR)
        rc = connect(*w, (struct sockaddr *) &addr, sizeof addr);

    //  Accept connection from writer.
    if (rc != SOCKET_ERROR)
        *r = accept(listener, NULL, NULL);

    //  Send/receive large chunk to work around TCP slow start
    //  This code is a workaround for #1608
    if (*r != INVALID_SOCKET)
    {
        size_t dummy_size = 1024 * 1024; //  1M to overload default receive buffer
        unsigned char *dummy = (unsigned char *)malloc(dummy_size);
        int still_to_send = (int)dummy_size;
        int still_to_recv = (int)dummy_size;
        while (still_to_send || still_to_recv)
        {
            int nbytes;
            if (still_to_send > 0)
            {
                nbytes =
                    ::send(*w, (char *)(dummy + dummy_size - still_to_send),
                    still_to_send, 0);
                CHECK(nbytes != SOCKET_ERROR);
                still_to_send -= nbytes;
            }
            nbytes = ::recv(*r, (char *)(dummy + dummy_size - still_to_recv),
                still_to_recv, 0);
            CHECK(nbytes != SOCKET_ERROR);
            still_to_recv -= nbytes;
        }
        free(dummy);
    }

    //  Save errno if error occurred in bind/listen/connect/accept.
    int saved_errno = 0;
    if (*r == INVALID_SOCKET)
        saved_errno = WSAGetLastError();

    //  We don't need the listening socket anymore. Close it.
    rc = closesocket(listener);
    CHECK(rc != SOCKET_ERROR);

    if (*r != INVALID_SOCKET)
    {
        //  On Windows, preventing sockets to be inherited by child processes.
        BOOL brc = SetHandleInformation((HANDLE)*r, HANDLE_FLAG_INHERIT, 0);
        CHECK(brc);
    }
    else
    {
        //  Cleanup writer if connection failed
        if (*w != INVALID_SOCKET) {
            rc = closesocket(*w);
            CHECK(rc != SOCKET_ERROR);
            *w = INVALID_SOCKET;
        }
        return -1;
    }
    return 0;
}
#endif

#if !defined(_WIN32)
static int make_pair_sockpair(int* r, int* w)
{
    int sv[2];
    int rc = socketpair (AF_UNIX, SOCK_STREAM, 0, sv);
    if (rc == -1)
    {
        CHECK(errno == ENFILE || errno == EMFILE);
        *w = *r = -1;
        return -1;
    }
    //  If there's no SOCK_CLOEXEC, let's try the second best option. Note that
    //  race condition can cause socket not to be closed (if fork happens
    //  between socket creation and this point).
    rc = fcntl (sv[0], F_SETFD, FD_CLOEXEC);
    CHECK (rc != -1);
    rc = fcntl (sv[1], F_SETFD, FD_CLOEXEC);
    CHECK(rc != -1);
    *w = sv[0];
    *r = sv[1];
    return 0;
}
#endif

int make_fdpair(SOCKET* r, SOCKET* w)
{
#if defined(_WIN32)
    return make_pair_winsock(r, w);
#elif defined(__linux__)
    return make_pair_eventfd(r, w);
#else
    return make_pair_sockpair(r, w);
#endif
}

void unblock_socket(SOCKET fd)
{
#ifdef _WIN32
    u_long nonblock = 1;
    int rc = ioctlsocket(fd, FIONBIO, &nonblock);
    CHECK(rc != SOCKET_ERROR);
#else
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1)
        flags = 0;
    int rc = fcntl(fd, F_SETFL, flags | O_NONBLOCK);
    CHECK(rc != -1);
#endif
}
