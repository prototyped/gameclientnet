// Copyright 2018-preset ichenq@outlook.com. All Rights Reserved.
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#ifdef _WIN32
#include <WinSock2.h>
#else
typedef int SOCKET;
#define INVALID_SOCKET  (-1)
#define SOCKET_ERROR    (-1)
#define INFINITE        0x7FFFFFFF
#endif

// Creates a pair of sockets (using signaler_port on OS using TCP sockets).
// Returns -1 if we could not make the socket pair successfully
int make_fdpair(SOCKET* r, SOCKET* w);

void unblock_socket(SOCKET fd);
