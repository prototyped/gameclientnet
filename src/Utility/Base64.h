/* -*- mode: c; c-basic-offset: 4; indent-tabs-mode: nil; tab-width: 4 -*- */
/* vi: set expandtab shiftwidth=4 tabstop=4: */

/**
 * \file
 * <PRE>
 * High performance base64 encoder / decoder
 * Version 1.3 -- 17-Mar-2006
 *
 * Copyright &copy; 2005, 2006, Nick Galbreath -- nickg [at] modp [dot] com
 * All rights reserved.
 *
 * http://modp.com/release/base64
 *
 * Released under bsd license.  See modp_b64.c for details.
 * </pre>
 *
 * The default implementation is the standard b64 encoding with padding.
 * It's easy to change this to use "URL safe" characters and to remove
 * padding.  See the modp_b64.c source code for details.
 *
 */

#pragma once

#include <stddef.h>
#include <string>
#include "Range.h"

/**
 * Given a source string of length len, this returns the amount of
 * memory the destination string should have.
 *
 * remember, this is integer math
 * 3 bytes turn into 4 chars
 * ceiling[len / 3] * 4 + 1
 *
 * +1 is for any extra null.
 */
#define base64_encode_len(A) ((A+2)/3 * 4 + 1)

/**
 * Given a base64 string of length len,
 *   this returns the amount of memory required for output string
 *  It maybe be more than the actual number of bytes written.
 * NOTE: remember this is integer math
 * this allocates a bit more memory than traditional versions of b64
 * decode  4 chars turn into 3 bytes
 * floor[len * 3/4] + 2
 */
#define base64_decode_len(A) (A / 4 * 3 + 2)

std::string& base64_encode(ByteRange bytes_to_encode, std::string& out);
std::string& base64_decode(StringPiece encoded_text, std::string& out);

inline std::string base64_encode(ByteRange data)
{
    std::string out;
	out.reserve(base64_encode_len(data.size()));
	return base64_encode(data, out);
}

inline std::string base64_encode(StringPiece sp)
{
	ByteRange br((const unsigned char*)sp.data(), sp.size());
	return base64_encode(br);
}

inline std::string base64_decode(StringPiece encodede_text)
{
	std::string out;
	out.reserve(base64_decode_len(encodede_text.size()));
	return base64_decode(encodede_text, out);
}
