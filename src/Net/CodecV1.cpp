﻿// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.


#include "CodecV1.h"
#include <zlib.h>
#include "Compression.h"
#include "Errno.h"
#include "Packet.h"
#include "Utility/Endian.h"
#include "Utility/Buffer.h"
#include "Utility/StringUtil.h"

namespace net {


CodecV1::CodecV1()
{
}

CodecV1::~CodecV1()
{
}

void CodecV1::SetCrypter(bool dec, ICrypter* crypter)
{
    assert(crypter);
    if (dec)
    {
        decrypter_ = crypter;
    }
    else 
    {
        encrypter_ = crypter;
    }
}

ICrypter* CodecV1::GetCrypter(bool dec)
{
    return dec ? decrypter_ : encrypter_;
}

// return how many bytes read
int CodecV1::Decode(const Buffer& buffer, Packet* pkt, int* readLen)
{
    size_t bytes = buffer.readableBytes();
    if (bytes < CodecV1HeaderBytes)
    {
        return ErrTryAgain;
    }
    const uint8_t* data = (const uint8_t*)buffer.peek();
    ICrypter* crypter = GetCrypter(true);
    if (crypter != nullptr)
    {
        crypter->Encrypt(data, CodecV1HeaderBytes, (char*)data, CodecV1HeaderBytes);
        crypter->UpdateIV();
    }

    uint32_t length = LittleEndian::GetUint32(data);
    length &= VersionBitsMask;
    if (length > CodecV1MaxPayload)
    {
        pkt->command = LittleEndian::GetUint16(data + 4);
        std::string text = prettyPrint(length, PRETTY_BYTES_IEC);
        LOG(ERROR) << stringPrintf("CodecV1::Decode: packet[%d] size[%s] too large", pkt->command, text.c_str());
        return ErrPacketSizeTooLarge;
    }
    if (bytes < length + CodecV1HeaderBytes)
    {
        return ErrTryAgain;
    }

    pkt->command = LittleEndian::GetUint16(data + 4);
    pkt->flags = LittleEndian::GetUint16(data + 6);
    pkt->sequence = LittleEndian::GetUint16(data + 8);
    pkt->node = LittleEndian::GetUint16(data + 10);
    pkt->status = LittleEndian::GetUint32(data + 12);

    if (readLen != nullptr)
    {
        *readLen = (CodecV1HeaderBytes + length);
    }
    data += CodecV1HeaderBytes;

    if ((pkt->flags & PacketFlagError) != 0) // an error notify
    {
        return ErrOK;
    }
    if (length > 0)
    {
        uint32_t checksum = crc32(0, (const Bytef*)data, length);
        if (checksum != pkt->status)
        {
            LOG(ERROR) << ("CodecV1: packet[{}] checksum mismatch: {} != {}", pkt->command, pkt->status, checksum);
            return ErrPacketChecksumMismatch;
        }
        BufferPtr inflated;
        ByteRange buf(data, length);
        if ((pkt->flags & PacketFlagCompressed) > 0)
        {
            inflated = compress_.UnCompress(buf);
            buf = inflated->toStringPiece();
        }
    }
    return ErrOK;
}

int CodecV1::Encode(Packet* pkt, Buffer* buffer)
{
    assert(pkt != nullptr && buffer != nullptr);
    std::vector<char> content;
    pkt->SerializeTo(content);
    if (content.size() > CodecV1MaxPayload)
    {
        auto strSize = prettyPrint(content.size(), PRETTY_BYTES_IEC);
        LOG(ERROR) << stringPrintf("CodecV1: packet[%d] size[%s] too large", pkt->command, strSize.c_str());
        return ErrPacketSizeTooLarge;
    }
    int32_t status = 0;
    uint16_t flags = pkt->flags;
    flags &= PacketFlagBitsMask;
    flags |= PacketFlagTypeProto;
    ByteRange body = StringPiece(content);
    uint32_t length = (uint32_t)body.size();
    BufferPtr deflated;
    if (length > CodecV1CompressThreshold)
    {
        deflated = compress_.Compress(body);
        body = deflated->toStringPiece();
        length = (uint32_t)body.size();
        flags |= PacketFlagCompressed;
    }
    else
    {
        flags |= PacketFlagPlain;
    }
    if (length > 0)
    {
        status = crc32(0, body.data(), body.size());
    }

    // high 4 bits is reserved for protocol version
    length |= ((uint32_t)CodecV1ProtocolVersion << VersionBitsShift);
    buffer->appendInt32(length);
    buffer->appendInt16(pkt->command);
    buffer->appendInt16(flags);
    buffer->appendInt16(pkt->sequence);
    buffer->appendInt16(pkt->node);
    buffer->appendInt32(status);
    ICrypter* crypter = GetCrypter(false);
    if (crypter != nullptr)
    {
        char* buf = (char*)buffer->peek();
        crypter->Encrypt(buf, CodecV1HeaderBytes, buf, CodecV1HeaderBytes);
        crypter->UpdateIV();
    }

    buffer->append(body);
    return ErrOK;
}

} // namespace net
