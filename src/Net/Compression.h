﻿// Copyright © 2016-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.
#pragma once

#include <memory>
#include <vector>
#include "Utility/Buffer.h"

namespace net {

// Zlib compression and decompression
class Compression
{
public:
    enum 
    {
        COMPRESSION_LEVEL_DEFAULT = 0,
        COMPRESSION_LEVEL_FASTEST = 1,
        COMPRESSION_LEVEL_BEST = 2,
    };
public:
    explicit Compression(int level = COMPRESSION_LEVEL_DEFAULT);
    ~Compression();

    Compression(const Compression&) = delete;
    Compression& operator = (const Compression&) = delete;

    // compresss data range into a buffer object
    BufferPtr Compress(ByteRange data);

    // uncompress data range into a buffer object
    BufferPtr UnCompress(ByteRange data);

private:
    int level_ = 0;
    std::vector<uint8_t>   buffer_;
};

} // namespace net