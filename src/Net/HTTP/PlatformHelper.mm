#include "stdafx.h"
#include "PlatformHelper.h"
#import <Foundation/Foundation.h>
#import "ViewController.h"
#import <CoreMotion/CoreMotion.h>
#import "GameKitHelper.h"
#import "ReplayKitTool/BQPReplaykit.h"
#include <iostream>
#include <fstream>
#include "StarRaidersCenter.h"
#include "Utility/BeatsUtility/FilePathTool.h"
#include "EnginePublic/BeyondEngineVersion.h"
#include "Config/ServerConfig.h"
#include "Language/LanguageManager.h"
#include "StarRaidersCenter.h"
#include <sys/stat.h>
#include <dirent.h>

CPlatformHelper* CPlatformHelper::m_pInstance = NULL;
CMMotionManager* g_motionManager = nil;
CPlatformHelper::CPlatformHelper()
{
    
}

CPlatformHelper::~CPlatformHelper()
{
    [g_motionManager release];
    g_motionManager = nil;
}

bool CPlatformHelper::IsUpSideDown()
{
    return [[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeRight;
}

void CPlatformHelper::InitGameExtrat()
{
    std::string strResVersion = fmt::format( _T("{}.{}.{}"), BEYONDENGINE_VER_MAJOR, BEYONDENGINE_VER_MINOR, BEYONDENGINE_VER_REVISION);
    CStarRaidersCenter::GetInstance()->SetAppVersion(strResVersion);
    SetAssetFileExtracted(!IsNeedExtract());
}

std::string CPlatformHelper::GetResourceRootPath()
{
    if (m_strResourcePath.empty())
    {
        m_strResourcePath = [[[NSBundle mainBundle] resourcePath] UTF8String];
    }
    return m_strResourcePath;
}

const char* CPlatformHelper::GetPersistentDataPath()
{
    if (m_strPersistentPath.empty())
    {
        NSString* path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
        const char* pPath = [path cStringUsingEncoding:NSUTF8StringEncoding];
        m_strPersistentPath = pPath;
    }
    return m_strPersistentPath.c_str();
}

std::string CPlatformHelper::GetBinaryPath()
{
    return GetResourceRootPath();
}

bool CPlatformHelper::IsAssetFileExtracted() const
{
    return m_bExacted;
}

void CPlatformHelper::SetAssetFileExtracted(bool bExacted)
{
    m_bExacted = bExacted;
}

bool CPlatformHelper::IsNeedExtract()
{
    bool bNeedExact = false;
    TString strVersionFileName = GetPersistentDataPath();
    strVersionFileName.append("/").append("ver.bin");
    FILE* pVerFile = _tfopen(strVersionFileName.c_str(), "rb");
    if (pVerFile != nullptr)//First time run our game.
    {
        char verBuffer[128];
        fgets(verBuffer, 128, pVerFile);
        BEATS_PRINT("Find version file {} version:{}\n", strVersionFileName.c_str(), verBuffer);
        fclose(pVerFile);
        
        TString strVersionName = CStarRaidersCenter::GetInstance()->GetAppVersion();
        if (strcmp(verBuffer, strVersionName.c_str()) != 0)
        {
            bNeedExact = true;
        }
        else
        {
            SetAssetFileExtracted(true);
        }
    }
    else
    {
        bNeedExact = true;
    }
    return bNeedExact;
}

void CPlatformHelper::ShowLoginDialog()
{
    NSString *strOK = NSLocalizedString(@"ok",@"");
    NSString *strMsg = NSLocalizedString(@"inputAccountPassword",@"");
    ViewController* pViewController = (ViewController*)m_pViewController;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:strMsg delegate:pViewController cancelButtonTitle:strOK otherButtonTitles:nil, nil];
    alert.tag = TAG_LOGIN_ALERT;
    alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    [alert show];
    [alert release];
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    
    UITextField* accountField = [alert textFieldAtIndex:0];
    UITextField* passwordField = [alert textFieldAtIndex:1];
    accountField.keyboardType = UIKeyboardTypeASCIICapable;
    
    [accountField setText:[user objectForKey:@"account"]];
    [passwordField setText:[user objectForKey:@"password"]];
}


void CPlatformHelper::ShowMessage(const std::string& strMsg)
{
    NSString *strTip = [NSString stringWithUTF8String:strMsg.c_str()];
    [(ViewController*)m_pViewController showMessage:strTip];
}

void CPlatformHelper::FadeOutMessage(float fTime)
{
    [(ViewController*)m_pViewController hideMessage:fTime];
}

void CPlatformHelper::ShowAppraise()
{
    NSString *strAppraise = NSLocalizedString(@"appraise",@"");
    NSString *strOK = NSLocalizedString(@"YES",@"");
    NSString *strCancel = NSLocalizedString(@"NO",@"");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:strAppraise delegate:(ViewController*)m_pViewController cancelButtonTitle:strOK otherButtonTitles:strCancel, nil];
    alert.tag = TAG_APPRAISE;
    [alert show];
    [alert release];
}

void CPlatformHelper::ShowLoadingAnimation(int ntime)
{
    [NSTimer scheduledTimerWithTimeInterval:ntime target:(ViewController*)m_pViewController selector:@selector(requestPayTimeOut) userInfo:nil repeats:NO];
    
    UIActivityIndicatorView *indicator = nil;
    indicator = (UIActivityIndicatorView *)[((ViewController*)m_pViewController).view viewWithTag:TAG_REQUEST_PAY];
    if (indicator == nil) {
        //初始化:
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        indicator.tag = TAG_REQUEST_PAY;
        //设置显示样式,见UIActivityIndicatorViewStyle的定义
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        //设置背景色
        indicator.backgroundColor = [UIColor blackColor];
        //设置背景透明
        indicator.alpha = 0.5;
        //设置背景为圆角矩形
        indicator.layer.cornerRadius = 6;
        indicator.layer.masksToBounds = YES;
        //设置显示位置
        [indicator setCenter:CGPointMake(((ViewController*)m_pViewController).view.frame.size.width / 2.0, ((ViewController*)m_pViewController).view.frame.size.height / 2.0)];
        //开始显示Loading动画
        [indicator startAnimating];
        [((ViewController*)m_pViewController).view addSubview:indicator];
        [indicator release];
    }
    //开始显示Loading动画
    [indicator startAnimating];
}

void CPlatformHelper::CloseLoadingAnimation()
{
    dispatch_async(dispatch_get_main_queue(), ^{
    UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)[((ViewController*)m_pViewController).view viewWithTag:TAG_REQUEST_PAY];
    [indicator stopAnimating];
    });
}

void CPlatformHelper::DisableScreenSleep(bool bDisable)
{
    if (bDisable) {
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    }
    else{
        [UIApplication sharedApplication].idleTimerDisabled = NO;
    }
}

std::string CPlatformHelper::GetDeviceIMEI()
{
    NSString *idfv = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    const char* pUUID = [idfv cStringUsingEncoding:NSUTF8StringEncoding];
    return pUUID;
}

std::string CPlatformHelper::GetCurrentSystemLanguageName() const
{
    NSArray *appLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    NSString *languageName = [appLanguages objectAtIndex:0];
    return [languageName UTF8String];
}

void CPlatformHelper::SetPlatformFPS(int nFPS)
{
    [(ViewController*)m_pViewController setfps:nFPS];
}

//单位M
int CPlatformHelper::GetDiskFreeSpace()
{
    int nFreeSpace = -1;
    NSError *error = nil;  
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);  
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];  
    if (dictionary)  
    { 
        NSNumber *_free = [dictionary objectForKey:NSFileSystemFreeSize];  
        nFreeSpace = [_free unsignedLongLongValue]*1.0/(1024 * 1024);
    } 
    else  
    {  
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);  
    }  
    return nFreeSpace;
}

void CPlatformHelper::OpenDownloadWeb()
{
    auto config = ServerConfig::GetInstance()->GetDefault();
    std::string strWeb = config.strAppUpdateUrl;
    NSString *urlText = [NSString stringWithUTF8String:strWeb.c_str()];
    [[UIApplication sharedApplication] openURL:[ NSURL URLWithString:urlText]];
}

void CPlatformHelper::InitSenorsManager()
{
    BEATS_ASSERT(g_motionManager == nil);
    g_motionManager = [[CMMotionManager alloc] init];
}

void CPlatformHelper::StartAccelerometerListener()
{
    BEATS_ASSERT(g_motionManager);
    if (g_motionManager.accelerometerAvailable) {
        g_motionManager.accelerometerUpdateInterval = 1.f/60.f;
        [g_motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue mainQueue]
                                withHandler:^(CMAccelerometerData *data, NSError *error) {
            OnAccelerometer(data.acceleration.x, data.acceleration.y, data.acceleration.z);
        }];
    }
}

void CPlatformHelper::StopAccelerometerListener()
{
    BEATS_ASSERT(g_motionManager);
    [g_motionManager stopAccelerometerUpdates];
}

void CPlatformHelper::OnAccelerometer(float x, float y, float z)
{
    x = MAX(x, -1.0f);
    x = MIN(x, 1.0f);
    y = MAX(y, -1.0f);
    y = MIN(y, 1.0f);
    z = MAX(z, -1.0f);
    z = MIN(z, 1.0f);
    m_vecAccelerometer = CVec3(x, y, z);
}

const CVec3& CPlatformHelper::GetAccelerometer() const
{
    return m_vecAccelerometer;
}


void CPlatformHelper::StartGravityListener()
{
    BEATS_ASSERT(g_motionManager);
    if (g_motionManager.accelerometerAvailable) {
        g_motionManager.accelerometerUpdateInterval = 1.f/30.f;
        [g_motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue]
                                withHandler:^(CMDeviceMotion *data, NSError *error) {
            OnGravity(data.gravity.x, data.gravity.y, data.gravity.z);
        }];
    }
}

void CPlatformHelper::StopGravityListener()
{
    BEATS_ASSERT(g_motionManager);
    [g_motionManager stopDeviceMotionUpdates];
}

void CPlatformHelper::OnGravity(float x, float y, float z)
{
    x = MAX(x, -1.0f);
    x = MIN(x, 1.0f);
    y = MAX(y, -1.0f);
    y = MIN(y, 1.0f);
    z = MAX(z, -1.0f);
    z = MIN(z, 1.0f);
    m_vecGravity = CVec3(x, y, z);
}

const CVec3& CPlatformHelper::GetGravity() const
{
    return m_vecGravity;
}

// send HTTP request to `strUrl`, and retrieve reponse data to `strData`
int CPlatformHelper::HttpRequestGet(const std::string& strUrl, std::string& strData, std::string& strErr)
{
    NSString *urlString= [NSString stringWithCString:strUrl.c_str() encoding:[NSString defaultCStringEncoding]];
    NSLog(@"HttpRequestGet start %@", urlString);
    //NSString *urlString = [NSString stringWithString:strUrl];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    //[request setHTTPMethod:@"GET"];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSLog(@"HttpRequestGet: Firing synchronous url connection...");
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if ([data length] > 0 && error == nil) {
        NSLog(@"HttpRequestGet: %lu bytes of data was returned.",(unsigned long)[data length]);
        NSLog(@"HttpRequestGet: %@", data);
        const void* pdata = [data bytes];
        strData.assign((const char*)pdata, data.length);
    }else if([data length] == 0 && error == nil){
        NSLog(@"HttpRequestGet: No data was return.");
        strErr = "no data";
    }else if (error != nil){
        NSLog(@"HttpRequestGet: Error happened = %@", error);
        strErr = [[error localizedDescription] UTF8String];
    }
    NSLog(@"HttpRequestGet done.");
    return 0;
}

// send HTTP post request to `strUrl` with `strPostdata`, and retrieve reponse data to `strData`
int CPlatformHelper::HttpRequestPost(const std::string& strUrl, const std::string& strPostdata, std::string& strData, std::string& strErr)
{
    NSString *urlString= [NSString stringWithCString:strUrl.c_str() encoding:[NSString defaultCStringEncoding]];
    NSLog(@"HttpRequestPost start %@", urlString);
    NSData *postData = [NSData dataWithBytes:strPostdata.c_str() length:strPostdata.length()];
    NSString *postLength = [NSString stringWithFormat:@"%ld", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if ([data length] > 0 && error == nil) {
        NSLog(@"HttpRequestPost： %lu bytes of data was returned.",(unsigned long)[data length]);
        NSLog(@"%@", data);
        const void* pdata = [data bytes];
        strData.assign((const char*)pdata, data.length);
    }else if([data length] == 0 && error == nil){
        NSLog(@"HttpRequestPost： No data was return.");
        strErr = "no data";
    }else if (error != nil){
        NSLog(@"Error happened = %@", error);
        strErr = [[error localizedDescription] UTF8String];
    }
    NSLog(@"HttpRequestPost done.");
    return 0;
}

#if (BEYONDENGINE_PLATFORM == PLATFORM_IOS)

//  network status
//  NotReachable = 0,
//  ReachableViaWiFi,
//  ReachableViaWWAN
void CPlatformHelper::NetWorkChanged(int32_t nStatus)
{
    BEATS_PRINT("network change to {}",nStatus);
    CStarRaidersCenter::GetInstance()->OnNetworkChanged(nStatus);
}

void CPlatformHelper::ClickedBroadcastBtn()
{
    if ([[BQPReplaykit sharedInstance] isVersionOK])
    {
        ViewController * con = (ViewController*)[UIApplication sharedApplication].keyWindow.rootViewController;
        [con onBroadcastClicked];
    }
    else
    {
        NSString *strNotSupport = NSLocalizedString(@"notsupport",@"");
        ViewController * con = (ViewController*)[UIApplication sharedApplication].keyWindow.rootViewController;
        [con showMessage:strNotSupport];
        [con hideMessage:1.0f];
    }
}

bool CPlatformHelper::LoginGameCenter(){
    return [[GameKitHelper sharedGameKitHelper] authenticateLocalUser];
}

void CPlatformHelper::ShowGameCenter(){
    [[GameKitHelper sharedGameKitHelper] showLeaderboard];
}

void CPlatformHelper::ReportScore(int nScore){
    if (LoginGameCenter()) {
        [[GameKitHelper sharedGameKitHelper] submitScore:(int64_t)nScore category:RANDK_POPULACE_KEY];
    }
}

void CPlatformHelper::ReportAchive(float fPercent,const std::string& strName)
{
    std::string strNameEx = "." + strName;
    NSString* strNameUtf8 = [NSString stringWithUTF8String:strNameEx.c_str()];
    NSString* strIdentifier = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"] stringByAppendingString:strNameUtf8];
    [[GameKitHelper sharedGameKitHelper] reportAchievementIdentifier:strIdentifier  percentComplete:fPercent];
}

bool CPlatformHelper::IsFolder(const std::string& path)
{
   struct stat st; 
   int ret = stat(path.c_str(), &st);
   return ret>=0 && S_ISDIR(st.st_mode);
}
 
bool CPlatformHelper::IsFile(const std::string& path)
{
   struct stat st; 
   int ret = stat(path.c_str(), &st);
   return ret>=0 && S_ISREG(st.st_mode);
}

bool CPlatformHelper::RemoveDir(const std::string& path)
{
    struct dirent* ent = NULL;
    DIR* pDir;
    pDir = opendir(path.c_str());
    while(NULL != (ent = readdir(pDir)))
    {
        std::string fullpath = path + "/" + ent->d_name;
        if(IsFolder(fullpath))
        {
            if(strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0)
            {
                RemoveDir(fullpath);
            }
        }
        else if(IsFile(fullpath))
        {
            remove(fullpath.c_str());
        }
    }
    closedir(pDir);
    rmdir(path.c_str());
    return  true;
}

void CPlatformHelper::ShowKeyboard(bool bShow)
{
    if(m_pViewController != nullptr)
    {
        BeyondView* pBeyondView = (BeyondView*)((ViewController*)m_pViewController).view;
        if (bShow)
        {
            [pBeyondView ShowKeyboard];
        }
        else
        {
            [pBeyondView HideKeyboard];
        }
    }
}

#endif
