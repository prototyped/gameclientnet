﻿// Copyright © 2018-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#include "HttpClient.h"
#include <string.h>
#include <chrono>
#include <thread>
#include <fstream>
#include <vector>
#include <atomic>
#include <mutex>
#include "Utility/Conv.h"
#include "Utility/StringUtil.h"
#include "Utility/ScopeGuard.h"
#include "Net/IPAddress.h"
#include "Platform/PlatformHelper.h"
#include "HttpStatus.h"
#include "Uri.h"

enum
{
    MAX_RETRY_TIMES = 3,
};


namespace net {


struct HttpClientImpl
{
    mutable int running_ = 0;   // is worker running now
    mutable int shutdown_ = 0;  // stop worker signal

    std::atomic<uint32_t>   content_bytes_ { 0 };
    std::mutex  req_mut;
    std::mutex  res_mut;
    std::vector<HTTPClient::RequestContextPtr>   request_chan_;
    std::vector<HTTPClient::RequestContextPtr>   response_chan_;
};

static HttpClientImpl*  impl_ = nullptr;


void HTTPClient::Initialize()
{
    if (impl_ != nullptr)
    {
        Release();
    }
	PlatformHelper::MustInitializeWinInet();
    impl_ = new HttpClientImpl;
    Start();
}

void HTTPClient::Release()
{
    Stop();
	PlatformHelper::UnInitializeWinInet();
    delete impl_;
    impl_ = nullptr;
}


////////////////////////////////////////////////////////////////////

// Get downloaded content bytes size
int HTTPClient::ContentBytes()
{
    return impl_->content_bytes_.load();
}

void HTTPClient::ResetContentByte()
{
    impl_->content_bytes_.store(0);
}

bool HTTPClient::Get(RequestContextPtr request, HTTPRequestCallback cb)
{
    DCHECK(!request->basepath.empty());
    request->callback = cb;
    request->method = "GET";

    std::lock_guard<std::mutex> guard(impl_->req_mut);
    impl_->request_chan_.push_back(request);
    return true;
}

bool HTTPClient::Post(RequestContextPtr request, HTTPRequestCallback cb)
{
	DCHECK(!request->basepath.empty());
    request->callback = cb;
    request->method = "POST";

    std::lock_guard<std::mutex> guard(impl_->req_mut);
    impl_->request_chan_.push_back(request);
    return true;
}

bool HTTPClient::DownloadFile(RequestContextPtr request, HTTPRequestCallback cb)
{
    request->callback = cb;
    request->method = "GET";
    if (request->filepath.empty())
    {
        request->filepath = GetUrlFilePathName(request->querypath);
    }

    // Add to HTTP request queue
    std::lock_guard<std::mutex> guard(impl_->req_mut);
    impl_->request_chan_.push_back(request);
    return true;
}

void HTTPClient::Start()
{
    if (impl_->running_)
    {
        LOG(WARNING) << "HTTPClient worker is already running";
        return;
    }

    impl_->running_ = 1;
    impl_->shutdown_ = 0;

    std::thread thrd(std::bind(&HTTPClient::WorkerLoop));
    thrd.detach();
}

void HTTPClient::Stop()
{
    int isShutdown = impl_->shutdown_;
    impl_->running_ = 0;
    if (isShutdown)
    {
        return;
    }
    impl_->shutdown_ = 1;
}


void HTTPClient::ConsumeRequest(RequestContextPtr req)
{
    SCOPE_EXIT
    {
        std::lock_guard<std::mutex> guard(impl_->res_mut);
        impl_->response_chan_.push_back(req);
    };

    try
    {
        RunRequest(req);
    }
    catch (std::exception& ex)
    {
        req->errText = ex.what();
        LOG(ERROR) << "exception" << typeid(ex).name() << ":" << ex.what();
    }
}


// runs in worker thread
void HTTPClient::WorkerLoop()
{
    for (;;)
    {
        if (impl_ == nullptr || impl_->shutdown_) // stop signal
        {
            break;
        }
        std::vector<RequestContextPtr> requests;
        {
            std::lock_guard<std::mutex> guard(impl_->req_mut);
            requests.swap(impl_->request_chan_);
        }
        if (requests.empty())
        {
            // sleep a while on idle
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            continue;
        }
        for (auto iter = requests.begin(); iter != requests.end(); ++iter)
        {
            ConsumeRequest(*iter);
        }
    }
    LOG(INFO) << "HTTPClient worker thread stopped";
}

void HTTPClient::Run()
{
    if (impl_ == nullptr || !impl_->running_)
    {
        return;
    }
    std::vector<RequestContextPtr> ready;
    {
        std::lock_guard<std::mutex> guard(impl_->res_mut);
        ready.swap(impl_->response_chan_);
    }
    for (auto iter = ready.begin(); iter != ready.end(); ++iter)
    {
        RequestContextPtr resp = *iter;
        if (resp && resp->callback)
        {
            (resp->callback)(resp);
        }
    }
}


void HTTPClient::RunRequest(RequestContextPtr req)
{
    std::string url = CatenateUrlPath(req->basepath, req->querypath);
    for (int i = 0; i < MAX_RETRY_TIMES; i++)
    {
        if (req->method == "POST")
        {
			PlatformHelper::HttpRequestPost(url, req->body, req->content, req->errText);
        }
        else
        {
			PlatformHelper::HttpRequestGet(url, req->content, req->errText);
        }
        if (req->errText == "")
        {
            if (!req->filepath.empty()) // write download content to file system
            {
				auto strBytes = prettyPrint(req->content.size(), PRETTY_BYTES);
                LOG(INFO) << stringPrintf("start write download content of %s bytes to file system %s", strBytes.c_str(), req->filepath.c_str());
                FILE* fp = fopen(req->filepath.c_str(), "wb");
                if (fp != NULL)
                {
                    fwrite(req->content.data(), req->content.size(), 1, fp);
                    fclose(fp);
					LOG(INFO) << stringPrintf("complete write download content to file system %s", req->filepath.c_str());
                }
                else
                {
                    LOG(ERROR) << stringPrintf("write download content to file system %s failed, error is {}", req->filepath.c_str(), errno);
                }
            }
            break;
        }
        else
        {
            // sleep a while and try again
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    }
}

std::string HTTPClient::GetUrlFilePathName(const std::string& url)
{
    std::string filename;
    std::string uri = url;
    std::replace(uri.begin(), uri.end(), '\\', '/');
    auto ipos = uri.rfind('/');
    if (ipos != std::string::npos)
    {
        filename = uri.substr(ipos + 1, uri.size());
    }
    return filename;
}

std::string HTTPClient::CatenateUrlPath(const std::string& url, const std::string& path)
{
    std::string uri = url;
    if (path.empty())
    {
        return uri;
    }
    if (uri.empty())
    {
        return path;
    }
    std::replace(uri.begin(), uri.end(), '\\', '/');
    if (*uri.rbegin() == '/')
    {
        if (*path.begin() == '/')
        {
            uri.pop_back();
        }
    }
    else
    {
        if (*path.begin() != '/')
        {
            uri += '/';
        }
    }
    return uri + path;
}

void HTTPClient::HandleError(RequestContextPtr req, const std::string& strTitle, const std::string& strContent)
{
    std::string title = strTitle;
    std::string text = strContent;
    std::string url = CatenateUrlPath(req->basepath, req->querypath);
    if (text == "")
    {
        text = req->errText;
    }
    text.append("\nurl " + url);
    // TO-DO:
}

} // namespace net
