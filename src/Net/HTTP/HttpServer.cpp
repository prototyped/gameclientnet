// Copyright 2015-2018 Beyondtech, Inc. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form 
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially 
// exploit the content. Nor may you transmit it or store it in any other website or 
// other form of electronic retrieval system.

#include "HttpServer.h"

#if defined(_WIN32)
#include "Platform/win32/HttpServerImplWin32.hxx"
#elif defined(__linux__)
#include "Platform/linux/HttpServerImplLinux.hxx"
#else
#pragma message("this platform does not implement http server yet")
#endif

