﻿// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include <string>
#include <memory>
#include <vector>
#include <functional>

namespace net {

enum 
{
    ENCODING_NONE = 0,
    ENCODING_DEFLATE = 1,
    ENCODING_GZIP = 2,
};

class HTTPClient
{
public:
    struct RequestContext;
    typedef std::unique_ptr<std::iostream>          StreamPtr;
    typedef std::shared_ptr<RequestContext>         RequestContextPtr;
    typedef std::function<void(RequestContextPtr)>  HTTPRequestCallback;
    typedef std::vector<std::string>                HeadFields;

    struct RequestContext
    {
        HTTPRequestCallback callback;       // complete callback
        std::string         basepath;       // 
        std::string         method;         // "GET" or "POST"
        std::string         querypath;      // download file path
        std::string         body;           // Post body
        std::string         content;        // response content
        std::string         errText;        // error string
        std::string         filepath;       // where to store the download content
    };

public:
    HTTPClient(const HTTPClient&) = delete;
    HTTPClient& operator=(const HTTPClient&) = delete;

    static void Initialize();
    static void Release();

    static void Start();
    static void Stop();
    static void Run();

    // Get downloaded content bytes size
    static int ContentBytes();

    static void ResetContentByte();

    // HTTP Get request
    static bool Get(RequestContextPtr request, HTTPRequestCallback cb);

    static bool Post(RequestContextPtr request, HTTPRequestCallback cb);

    // Download file from HTTP server
    static bool DownloadFile(RequestContextPtr request, HTTPRequestCallback cb);

public:
    static std::string GetUrlFilePathName(const std::string& url);
    static std::string CatenateUrlPath(const std::string& url, const std::string& path);
    static void HandleError(RequestContextPtr request, const std::string& title, const std::string& content);

private:
    static void ConsumeRequest(RequestContextPtr req);
    static void RunRequest(RequestContextPtr req);
    static void WorkerLoop();

};

} // namespace net
