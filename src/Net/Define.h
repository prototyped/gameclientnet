﻿// Copyright © 2018-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include <stdint.h>
#include <memory>
#include <vector>
#include <functional>
#include <unordered_map>
#include <system_error>


//服务类型
enum ServiceType : uint8_t
{
    SERVICE_CLIENT = 0x00,      //发送给客户端自己
    SERVICE_LOGIN = 0x02,       //登陆服务
    SERVICE_GATEWAY = 0x03,     //网关服务
};


namespace net {

enum
{
    // Flag bits
    PacketFlagPlain = 0x0001,           // message is plain text
    PacketFlagCompressed = 0x0002,      // message is compressed
    PacketFlagError = 0x0100,           // error encountered
    PacketFlagRpc = 0x0200,             //
    PacketFlagTypeProto = 0x1000,       //
    PacketFlagTypeText = 0x2000,        //

    PacketFlagBitsMask = 0xFF00,        //

#ifdef _DEBUG
    DefaultHeartBeatSec = 30,       // heartbeat interval, in seconds
    DefaultReplyTtl = 100,          // rpc timeout, in seconds
#else
    DefaultHeartBeatSec = 10,       // heartbeat interval, in seconds
    DefaultReplyTtl = 30,           // rpc timeout, in seconds
#endif
};

struct RpcContext;
typedef std::shared_ptr<RpcContext>  RpcContextPtr;

class Packet;
typedef std::shared_ptr<Packet>  PacketPtr;

typedef std::function<void(const std::error_code&)> ConnectionCallback;
typedef std::function<void(PacketPtr)> MessageCallback;

typedef std::vector<PacketPtr> PacketQueue;

typedef std::unordered_map<uint32_t, RpcContextPtr>     RpcCallContextMap;
typedef std::unordered_map<uint16_t, MessageCallback>   MessageHandlerMap;

}