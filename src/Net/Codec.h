﻿// Copyright © 2018-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include "Utility/Buffer.h"
#include "Packet.h"
#include "Compression.h"

namespace net {

enum
{
    ErrOK = 0,
    ErrTryAgain = -1,
    ErrPacketSizeTooLarge = -2,
    ErrPacketChecksumMismatch = -3,
    ErrParseProtoFail = -4,
};

class ICrypter
{
public:
    virtual ~ICrypter() {}
    
    virtual void Encrypt(const void* input, size_t inLen, void* out, size_t outLen) = 0;

    virtual void UpdateIV() = 0;
};

struct ICodec
{
public:
    virtual ~ICodec() {}

    virtual int HeaderBytes() = 0;

    // decode packet from bytes buffer, return how many bytes read
    virtual int Decode(const Buffer& buffer, Packet* pkt, int* readLen) = 0;

    // encode a packet to bytes buffer
    virtual int Encode(Packet* packet, Buffer* buf) = 0;

    virtual void SetCrypter(bool dec, ICrypter* crypter) = 0;
};

} // namespace net
