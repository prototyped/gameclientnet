﻿// Copyright © 2016-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#include "Transport.h"
#include <asio/write.hpp>
#include "Utility/Logging.h"


using namespace std::placeholders;


namespace net {

enum
{
    DefaultRecvBufferBytes = 2048,
    DefaultNewPacketBufBytes = 1024,
};


// codec should be dynamically allocated
Transport::Transport(asio::io_context& io_context, ICodec* codec)
    : socket_(io_context), codec_(codec), buf_(DefaultRecvBufferBytes)
{
    assert(codec != nullptr);
}

Transport::~Transport()
{
    LOG(INFO) << "Transport with socket " << socket_.native_handle() << " destruct";
    delete codec_;
    codec_ = nullptr;
}

std::error_code Transport::Connect(const std::string& addr, uint16_t port)
{
	std::error_code ec;
	auto address = asio::ip::address::from_string(addr, ec);
	if (!ec)
	{
		asio::ip::tcp::endpoint endpoint(address, port);
		socket_.connect(endpoint, ec);
		if (!ec)
		{
			connected_ = true;
		}
	}
	return ec;
}

bool Transport::Shutdown()
{
    std::error_code ec;
    socket_.shutdown(asio::ip::tcp::socket::shutdown_receive, ec);
    if (ec)
    {
        LOG(ERROR) << "shutdown_receive: " << ec.value() << ", " << ec.message();
        return false;
    }
    return true;
}

void Transport::Close()
{
    closing_ = true;
    std::error_code ec;
    if (connected_)
    {
        connected_ = false;
        if (!Shutdown())
        {
            return;
        }
        socket_.close(ec);
        if (ec)
        {
            LOG(ERROR) << "close socket: " << ec.value() << ", " << ec.message();
        }
    }
    incoming_ = nullptr;
    errors_ = nullptr;
    closing_ = false;
    connected_ = false;
}

void Transport::Start(PacketQueue* inqueue, ErrorQueue* errs)
{
    incoming_ = inqueue;
    errors_ = errs;
    started_ = true;
    StartRead();
}

void Transport::Write(PacketPtr packet)
{
    if (!connected_)
    {
        LOG(ERROR) << "write before connected";
        return;
    }
    auto buf = Buffer::create();
    int r = codec_->Encode(packet.get(), buf.get());
    if (r == ErrOK)
    {
        auto slice = buf->toStringPiece();
        if (!slice.empty())
        {
            asio::async_write(socket_,
                asio::buffer(slice.data(), slice.size()),
                std::bind(&Transport::OnWritten, this, _1, _2, buf));
        }
    }
    else
    {
        LOG(ERROR) << "Encode packet: " << packet->command << ", " << r;
    }
}

void Transport::StartRead()
{
    if (!connected_)
    {
        LOG(ERROR) << "read before connected";
        return;
    }
    buf_.ensureWritableBytes(DefaultNewPacketBufBytes); // make sure have enough space for next packet
    socket_.async_read_some(
        asio::buffer(buf_.beginWrite(), buf_.writableBytes()),
        std::bind(&Transport::OnRead, this, _1, _2));
}

void Transport::HandleIOError(const std::string& desc, const std::error_code& ec)
{
    connected_ = false;
    if (errors_)
    {
        errors_->push_back(Error(desc, ec)); // push to error signal queue
    }
}

void Transport::EnqueuePacket(PacketPtr pkt)
{
    if (incoming_)
    {
        incoming_->push_back(pkt); // push to incoming message queue
    }
}

void Transport::OnRead(const std::error_code& ec, size_t bytes)
{
    if (ec)
    {
        LOG(ERROR) << ec.value() << ", " << ec.message();
        HandleIOError(__FUNCTION__, ec);
        return;
    }

    buf_.hasWritten(bytes);
    while (buf_.readableBytes() >= (size_t)codec_->HeaderBytes())
    {
        auto pkt = Packet::create();
        int readLen = 0;
        int r = codec_->Decode(buf_, pkt.get(),&readLen);
        if (r == ErrOK)
        {
            buf_.retrieve(readLen);
            EnqueuePacket(pkt);
        }
        else
        {
            break;
        }
    }
    StartRead();
}


void Transport::OnWritten(const std::error_code& ec, size_t , BufferPtr buf)
{
    if (ec)
    {
        LOG(ERROR) << ec.value() << ", " << ec.message();
        HandleIOError(__FUNCTION__, ec);
    }
}

} // namespace net
