﻿// Copyright © 2016-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include <memory>
#include <system_error>
#include <asio/ip/tcp.hpp>
#include "Define.h"
#include "Codec.h"
#include "Error.h"

namespace net {

class Endpoint;


class Transport 
{
public:
    Transport(asio::io_context& io_context, ICodec* codec);
    ~Transport();

    Transport(const Transport&) = delete;
    Transport& operator = (const Transport&) = delete;

	std::error_code Connect(const std::string& addr, uint16_t port);

    void Start(PacketQueue* inqueue, ErrorQueue* errs);

    // Write data to peer
    void Write(PacketPtr packet);

    void Close();
    bool Shutdown();

    bool IsConnected() const { return connected_; }

    ICodec* GetCodec() { return codec_; }

private:
    void StartRead();
    void HandleIOError(const std::string& desc, const std::error_code& ec);
    void EnqueuePacket(PacketPtr pkt);

    void OnRead(const std::error_code& ec, size_t bytes);
    void OnWritten(const std::error_code& ec, size_t bytes, BufferPtr buf);

private:
    asio::ip::tcp::socket  socket_;
    PacketQueue*    incoming_ = nullptr;
    ErrorQueue*     errors_ = nullptr;
    ICodec*         codec_ = nullptr;
    Buffer          buf_;

    bool    started_ = false;
    bool    connected_ = false;
    bool    closing_ = false;
};

typedef std::shared_ptr<Transport>  TransportPtr;

} // namespace network
