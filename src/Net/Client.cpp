﻿// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#include "Client.h"
#include <sstream>
#include <typeinfo>
#include <chrono>
#include <random>
#include <vector>
#include <iterator>
#include <algorithm>
#include <asio/io_context.hpp>
#include <asio/steady_timer.hpp>
#include <asio/ip/address.hpp>
#include <google/protobuf/message.h>
#include <google/protobuf/util/json_util.h>
#include "Utility/Clock.h"
#include "Utility/Logging.h"
#include "Utility/StringUtil.h"
#include "Utility/ScopeGuard.h"
#include "Transport.h"
#include "IPAddress.h"
#include "proto/gateway.pb.h"


using namespace std::placeholders;


namespace net {

//////////////////////////////////////////////////////////////////////////

struct RpcContext
{
    uint16_t    command = 0;
    uint32_t    sequence = 0;
    int64_t     deadline = 0;
    MessageCallback cb;
};

struct Endpoint::InternalImpl
{
    InternalImpl(asio::io_context& ctx)
    {
        keep_aliver_.reset(new asio::steady_timer(ctx));
        reaper_.reset(new asio::steady_timer(ctx));

    }

    InternalImpl(const InternalImpl&) = delete;
    InternalImpl& operator=(const InternalImpl&) = delete;

    std::shared_ptr<Transport>          transport_;                 // transport object
    PacketQueue                         incoming_;                  // incoming packet queue
    ErrorQueue                          error_;                     // error signal
    std::unique_ptr<asio::steady_timer> keep_aliver_;               // heartbeating
    std::unique_ptr<asio::steady_timer> reaper_;                    // remote call reaper
    MessageHandlerMap                   handlers_;                  // message handlers
    RpcCallContextMap                   pending_;                   // <sequnce,handler>
    std::unordered_map<uint16_t, bool>  registry_;                  //
    bool                                is_ignore_error_ = false;   // ignore socket error
    bool                                is_keep_alive_ = false;     //
    uint8_t                             district_ = 0;              // default district 
    uint16_t                            last_expect_reply_ = 0;     //
    uint32_t                            last_sequence_ = 0;         //
    int64_t                             last_ping_ = 0;             // last time send ping
    int64_t                             last_pong_ = 0;             // last time recv pong
    uint32_t                            ping_count_ = 0;            // how many ping sent
    uint32_t                            pong_count_ = 0;            // how many pong recv
};

Endpoint::Endpoint(asio::io_context& ctx)
{
    impl_.reset(new Endpoint::InternalImpl(ctx));
}

Endpoint::~Endpoint()
{
}

void Endpoint::SetTransport(std::shared_ptr<Transport> transport)
{
    impl_->transport_ = transport;
}

std::shared_ptr<Transport> Endpoint::GetTransport()
{
    return impl_->transport_;
}

void Endpoint::SetDistrict(uint8_t district)
{
    impl_->district_ = district;
}

uint8_t Endpoint::GetDistrict() const
{
    return impl_->district_;
}

PacketQueue* Endpoint::IncomingQueue()
{
    return &impl_->incoming_;
}

ErrorQueue* Endpoint::Errors()
{
    return &impl_->error_;
}

void Endpoint::SetCodecEncrypter(bool dec, int method, const std::string& key, uint64_t iv)
{
    // TODO:
}

void Endpoint::SetLastSeqNo(uint32_t seqno)
{
    impl_->last_sequence_ = seqno;
}

bool Endpoint::IsWaitingReply() const
{
    return impl_->last_expect_reply_ > 0;
}

void Endpoint::SetIgnoreErrorFlag(bool bIgnore)
{
    impl_->is_ignore_error_ = bIgnore;
}

std::error_code Endpoint::Establish(asio::io_context& io_context, const std::string& address, ICodec* codec)
{
	std::vector<IPAddress> iplist;
	net::ParseHostAddress(address, &iplist);
	auto transport = std::make_shared<Transport>(io_context, codec);
	std::error_code ec = asio::error::invalid_argument;
	for (auto addr : iplist)
	{
		ec = transport->Connect(addr.first, addr.second);
		if (ec)
		{
			LOG(ERROR) << "Connect " << addr.first << ":" << addr.second << " error: " << ec.value();
			continue;
		}
		transport->Start(&impl_->incoming_, &impl_->error_);
		SetTransport(transport);
		break;
	}
	return ec;
}

void Endpoint::Close()
{
    impl_->pending_.clear();
    impl_->registry_.clear();
    impl_->handlers_.clear();
    if (impl_->transport_)
    {
        impl_->transport_->Close();
    }
}

void Endpoint::RegisterMessageCallback(MsgType command, MessageCallback callback)
{
    if (impl_->handlers_.count((uint16_t)command) > 0)
    {
        LOG(WARNING) << stringPrintf("duplicate message[%d] handler registration", command);
    }
    impl_->handlers_[(uint16_t)command] = callback;
}

void Endpoint::UnRegisterMessageCallback(MsgType command)
{
    impl_->handlers_.erase((uint16_t)command);
}


void Endpoint::Shutdown()
{
    impl_->error_.push_back(Error("heartbeat", asio::error::timed_out));
    if (impl_->transport_)
    {
        impl_->transport_->Shutdown();
    }
}

void Endpoint::Start(bool keepalive)
{
    if (keepalive)
    {
        impl_->is_keep_alive_ = true;
        impl_->last_ping_ = Clock::GetCurrentTimeMillis();
        impl_->keep_aliver_->expires_from_now(std::chrono::seconds(1));
        impl_->keep_aliver_->async_wait(std::bind(&Endpoint::OnKeepAlive, this, _1));

        RegisterMessageCallback(SM_HEARTBEAT_STATUS, [this](net::PacketPtr packet)
        {
            impl_->pong_count_++;
            impl_->last_pong_ = Clock::GetCurrentTimeMillis();
        });
    }
}

void Endpoint::OnKeepAlive(const std::error_code& ec)
{
    if (ec)
    {
        LOG(ERROR) << "Endpoint::OnKeepAlive: " << ec.value() << ", " << ec.message();
        return;
    }
    if (impl_->transport_ && impl_->transport_->IsConnected())
    {
        int64_t now = Clock::GetCurrentTimeMillis();
        if (now - impl_->last_ping_ > DefaultHeartBeatSec)
        {
            proto::HeartbeatReq msg;
            msg.set_time(time(NULL));
            Send(SERVICE_GATEWAY, CM_HEARTBEAT, &msg);
            impl_->ping_count_++;
            impl_->last_ping_ = now;
        }
    }
    impl_->keep_aliver_->expires_from_now(std::chrono::seconds(1));
    impl_->keep_aliver_->async_wait(std::bind(&Endpoint::OnKeepAlive, this, _1));
}

void Endpoint::Run()
{
    if (!impl_->incoming_.empty())
    {
        PacketQueue queue;
        impl_->incoming_.swap(queue);
        for (PacketPtr pkt : queue)
        {
            Execute(pkt);
        }
    }
    // handle error last, to make sure disconnect notify dispatched first
    if (!impl_->error_.empty()) // error happened
    {
        Error err = impl_->error_.front();
        impl_->error_.pop_front();
        HandlerError(err);
    }

    // TO-DO: application-level ping/heartbeat mechanism
    // iOS airplane mode issue, see https://sourceforge.net/p/asio/mailman/message/32310445/
    // if ping sent and pong expired, TCP connection may been closed
}

void Endpoint::HandlerError(const Error& err)
{
    if (impl_->is_ignore_error_)
    {
        return;
    }
    SCOPE_EXIT{ Close(); };
    

    // If we get error just after we set the game foreground (in 1 second)
    // we will restart the game at once without any message box.

}

void Endpoint::Execute(PacketPtr packet)
{
    uint32_t error = packet->GetError();
    if (error > 0 && error >= kRestartErrMin && error < kRestartErrMax) // check if fatal error happened
    {
        // restart app here
        return;
    }
    
    DumpPacketLog("Recv", packet);

    MessageCallback cb = impl_->handlers_[packet->command];
    if (cb == nullptr)
    {
        if (error > 0)
        {
            cb = impl_->handlers_[SM_NOTIFY_ERROR]; // redirect error callback
        }
    }
    if (cb == nullptr)
    {
        LOG(ERROR) << stringPrintf("message handler[{}] not found", packet->command);
        return;
    }

#ifndef _DEBUG
    try
    {
#endif
        cb(packet);
#ifndef _DEBUG
    }
    catch (std::exception& ex)
    {
        LOG(ERROR) << "message exception: " << packet->command << ", " << typeid(ex).name() << ", " << ex.what();
    }
#endif
}

void Endpoint::Send(ServiceType srv, MsgType command, const google::protobuf::Message* msg)
{
    PacketPtr packet = Packet::create();
    packet->node = (uint16_t)(srv << 8) | (uint16_t)impl_->district_;
    packet->sequence = impl_->last_sequence_;
    packet->command = (uint16_t)command;
    if (msg != nullptr)
    {
        std::string content = msg->SerializeAsString();
        packet->Append(content.data(), content.size());
    }
    if (srv == SERVICE_CLIENT) 
    {
        impl_->incoming_.push_back(packet); // sent to myself, loopback
    }
    else
    {
        impl_->transport_->Write(packet);
    }
    impl_->last_sequence_++;
    DumpPacketLog("Send", packet);
}

void Endpoint::Call(ServiceType srv, MsgType command, const google::protobuf::Message* msg, MessageCallback cb)
{    
    if (IsWaitingReply())
    {
        LOG_IF(WARNING, impl_->last_expect_reply_ == 0) << stringPrintf("last expected response[%d] not answered yet", impl_->last_expect_reply_);
    }
    AsyncCall(srv, command, msg, cb);
    impl_->last_expect_reply_ = command;
}

void Endpoint::AsyncCall(ServiceType srv, MsgType command, const google::protobuf::Message* msg, MessageCallback cb)
{
    assert(impl_->pending_.size() < USHRT_MAX);

    if (impl_->registry_.count(command) == 0)
    {
        impl_->registry_[command] = true;
        RegisterMessageCallback(command, std::bind(&Endpoint::OnRpcDone, this, _1));
    }
    int64_t now = Clock::GetCurrentTimeMillis();
    auto ctx = std::make_shared<RpcContext>();
    ctx->command = command;
    ctx->cb = cb;
    ctx->sequence = impl_->last_sequence_;
    ctx->deadline = now + DefaultReplyTtl;
    impl_->pending_[ctx->sequence] = ctx;
    Send(srv, command, msg);

    if (impl_->reaper_->expiry().time_since_epoch().count() == 0) // not started yet
    {
        impl_->reaper_->expires_from_now(std::chrono::seconds(1));
        impl_->reaper_->async_wait(std::bind(&Endpoint::ReapReply, this, _1));
    }
}

void Endpoint::ReapReply(const std::error_code& ec)
{
    if (ec)
    {
        LOG(ERROR) << "Endpoint::ReapReply: " << ec.value() << ", " << ec.message();
        return;
    }

    // repeat timer
    impl_->reaper_->expires_from_now(std::chrono::seconds(1));
    impl_->reaper_->async_wait(std::bind(&Endpoint::ReapReply, this, _1));

    if (impl_->pending_.empty())
    {
        return;
    }

    int64_t now = Clock::GetCurrentTimeMillis();
    while (true)
    {
        if (impl_->pending_.empty())
        {
            break;
        }
        auto pair = impl_->pending_.begin(); // front item should be fired first
        uint32_t errcode = 0;
        RpcContextPtr ctx = pair->second;
        if (now > ctx->deadline) // request call timed-out
        {
            //errcode = ErrRequestTimedout;
        }
        if (!impl_->transport_ || !impl_->transport_->IsConnected()) // connection lost during remote call
        {
            //errcode = eLTT_Game_Server_Connection_EOF_Message;
        }
        if (errcode == 0)
        {
            break;
        }
        else 
        {
            PacketPtr packet = Packet::create();
            packet->command = ctx->command;
            packet->status = errcode;
            packet->sequence = pair->first;
            packet->flags |= PacketFlagError;
            Execute(packet);
            LOG(WARNING) << stringPrintf("message: [%d] expired", packet->command);
        }
    }
}

void Endpoint::OnRpcDone(PacketPtr packet)
{
    //is waiting this reply
    if (impl_->last_expect_reply_ > 0 && packet->command == impl_->last_expect_reply_)
    {
        impl_->last_expect_reply_ = 0;
    }

    RpcContextPtr ctx = impl_->pending_[packet->sequence];
    impl_->pending_.erase(packet->sequence);
    if (ctx == nullptr)
    {
        LOG(WARNING) <<  stringPrintf("rpc context %d#%d not found", packet->command, packet->sequence);
        return;
    }
    if (ctx->cb == nullptr)
    {
        LOG(WARNING) << stringPrintf("rpc callback %d#%d  is null", packet->command, packet->sequence);
        return ;
    }
#ifndef _DEBUG
    try
    {
#endif
        ctx->cb(packet);
#ifndef _DEBUG
    }
    catch (std::exception& ex)
    {
        LOG(ERROR) << stringPrintf("message exception: %d, %s, %s", packet->command,  typeid(ex).name(), ex.what());
    }
#endif
}

void Endpoint::DumpPacketLog(const std::string& loc, PacketPtr packet)
{
    const char* arrow = "<-";
    const char* custom = "recv message";
    if (loc == "Send") {
        arrow = "->";
        custom = "send message";
    }
    MsgType msgid = MsgType(packet->command);
    std::string output;
    if (packet->GetError() == 0)
    {
        google::protobuf::Message* pMsg = NULL; // packet->GetMessage();
        if (pMsg != nullptr)
        {
            google::protobuf::util::MessageToJsonString(*pMsg, &output);
        }
    }
    LOG(INFO) << stringPrintf("%s %s %d: %s", custom, arrow, msgid, output.c_str());
}

} // namespace net
