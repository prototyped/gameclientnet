﻿// Copyright © 2016-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#include "IPAddress.h"
#include <chrono>
#include <thread>
#include "Utility/StringUtil.h"
#include "Utility/ScopeGuard.h"
#include <asio/ip/address.hpp>
#include <asio/ip/tcp.hpp>


enum
{
    MAX_RESOLVE_RETRY = 3,
};

namespace net {
    

static void ParseSockAddressToIP(const addrinfo* ai, std::vector<IPAddress>* iplist)
{
    assert(ai);
    void* inetAddr = nullptr;
    IPAddress address;
    switch (ai->ai_family)
    {
    case AF_INET:
        {
            const sockaddr_in* addr = (const sockaddr_in*)ai->ai_addr;
            address.second = ntohs(addr->sin_port);
            inetAddr = (void*)&addr->sin_addr;
        }
        break;

    case AF_INET6:
        {
            const sockaddr_in6* addr = (const sockaddr_in6*)ai->ai_addr;
            address.second = ntohs(addr->sin6_port);
            inetAddr = (void*)&addr->sin6_addr;
        }

    default:
        break;
    }

    if (inetAddr != nullptr)
    {
        char buffer[INET6_ADDRSTRLEN] = {};
        const char* ip = ::inet_ntop(ai->ai_family, inetAddr, buffer, ai->ai_addrlen);
        if (ip != NULL)
        {
            address.first = ip;
            iplist->push_back(address);
        }
        else
        {
            LOG(ERROR) << "inet_ntop: " << errno << ": " << strerror(errno);
        }
    }
}

int ResolveDomainNameToIP(const std::string& host, const std::string& service, std::vector<IPAddress>* iplist)
{
    assert(iplist);
    addrinfo hints = {};
    hints.ai_family = AF_UNSPEC;        // both IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM;    // TCP stream
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_ADDRCONFIG;

    addrinfo* ailist = nullptr;
    int ec = 0;
    for (int i = 0; i <= MAX_RESOLVE_RETRY; i++)
    {
        ec = ::getaddrinfo(host.data(), service.data(), &hints, &ailist);
        if (ec == EAI_AGAIN) // temporary failure, try again later
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10)); // sleep a while
            continue;
        }
        if (ec == 0)
        {
            addrinfo* ptr = ailist;
            while (ptr != nullptr)
            {
                ParseSockAddressToIP(ptr, iplist);
                ptr = ptr->ai_next;
            }
        }
        else
        {
            assert(ec);
            LOG(ERROR) << "getaddrinfo: " << ec << ", " << gai_strerror(ec);
        }
        if (ailist != nullptr)
        {
            ::freeaddrinfo(ailist);
        }
        break;
    }
    return ec;
}
    
    
// Is IP address or domain name
bool IsIPAddressFormat(StringPiece host)
{
    std::error_code ec;
    asio::ip::address::from_string(host.data(), ec);
    return ec.value() == 0;
}
    
// Algorithm taken from https://github.com/golang/go/blob/master/src/net/ipsock.go
ErrStatus SplitHostPort(StringPiece hostport, StringPiece* host, StringPiece* port)
{
    size_t j = 0;
    size_t k = 0;
    size_t i = hostport.rfind(':');
    if (i == std::string::npos)
    {
        *host = hostport;
        *port = "80";         // default HTTP port
        return eOK;
    }
    if (hostport[0] == '[')
    {
        size_t end = hostport.find(']'); // Expect the first ']' just before the last ':'.
        if (end == std::string::npos)
        {
            return eAddrErr; // missing ']' in address
        }
        if (end + 1 == hostport.size())
        {
            return eMissingPort; // There can't be a ':' behind the ']' now.
        }
        else if (end + 1 == i) // The expected result.
        {
        }
        else
        {
            // Either ']' isn't followed by a colon, or it is
            // followed by a colon that is not the last one.
            if (hostport[end + 1] == ':')
                return eTooManyColons;
            return eMissingPort;
        }
        *host = hostport.subpiece(1, end-1);
        j = 1;
        k = end + 1; // there can't be a '[' resp. ']' before these positions
    }
    else
    {
        *host = hostport.subpiece(0, i);
    }
    if (hostport.find('[', j) != std::string::npos)
    {
        return eAddrErr; // Unexpected '[' in address
    }
    if (hostport.find(']', k) != std::string::npos)
    {
        return eAddrErr; // Unexpected ']' in address
    }
    *port = hostport.subpiece(i + 1);
    return eOK;
}
    
static int ParseOneAddress(StringPiece address, std::vector<IPAddress>* iplist)
{
    StringPiece host;
    StringPiece port;
    if (SplitHostPort(address, &host, &port) != eOK)
    {
        LOG(ERROR) << "SplitHostAndPort: " << address;
        return EINVAL;
    }
    host = trimWhitespace(host);
    port = trimWhitespace(port);
    if (IsIPAddressFormat(host))
    {
        IPAddress ip = std::make_pair(host.str(), to<uint16_t>(port));
        iplist->push_back(ip);
        return 0;
    }
    else
    {
        return ResolveDomainNameToIP(host.str(), port.str(), iplist);
    }
}
    
void ParseHostAddress(StringPiece address, std::vector<IPAddress>* iplist)
{
    auto addrlist = Split(address, ",");
    for (auto addr : addrlist)
    {
        std::vector<IPAddress> vec;
        auto ec = ParseOneAddress(addr, &vec);
        if (!ec)
        {
            std::copy(vec.begin(), vec.end(), std::back_inserter(*iplist));
        }
        else
        {
            LOG(ERROR) << "ParseHostAddress[" << addr << "], " << ec << ": " << gai_strerror(ec);
        }
    }
}
    
} // namespace net
