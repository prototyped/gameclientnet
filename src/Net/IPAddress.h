﻿// Copyright © 2016-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include <stdint.h>
#include <utility>
#include <string>
#include <vector>
#include <system_error>
#include "Utility/Range.h"

namespace net {

typedef std::pair<std::string, uint16_t> IPAddress;

// error code of SplitHostPort
enum ErrStatus
{
    eOK,
    eMissingPort,
    eTooManyColons,
    eMissingBrackets,
    eAddrErr,
};


// DSN resolve, resolve domain name to IP address list
int ResolveDomainNameToIP(const std::string& host, const std::string& port, std::vector<IPAddress>* iplist);


// Check where an address is digit dotted IP address or domain name else.
bool IsIPAddressFormat(StringPiece host);

// Splits a network address of the form "host:port", "[host]:port" 
// or "[ipv6-host%zone]:port" into host or ipv6-host%zone and port.  
// A literal address or host name for IPv6 must be enclosed in square brackets, 
// as in "[::1]:80", "[ipv6-host]:http" or "[ipv6-host%zone]:80".
ErrStatus SplitHostPort(StringPiece hostport, StringPiece* host, StringPiece* port);


// Parse host address to `IPAddress` objects
// `address` can be domain name list eg. "patch.n.m.youzu.com, bd.sg.uuzu.com",
// or IP list eg. "122.226.211.154, 122.226.211.155:8080",
// or combined eg. "patch.n.m.youzu.com:8080, 122.226.211.154",
void ParseHostAddress(StringPiece address, std::vector<IPAddress>* iplist);


} // namespace net
