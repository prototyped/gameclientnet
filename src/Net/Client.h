﻿// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.


#pragma once

#include <string>
#include <memory>
#include <functional>
#include <type_traits>
#include <unordered_map>
#include <system_error>
#include <asio/io_context.hpp>
#include "MsgType.h"
#include "Error.h"
#include "Packet.h"
#include "Codec.h"

namespace net {

class Transport;

//
// Endpoint abstract a message channel to peer
//
class Endpoint
{
public:
    explicit Endpoint(asio::io_context& ctx);
    ~Endpoint();

    Endpoint(const Endpoint&) = delete;
    Endpoint& operator = (const Endpoint&) = delete;

    void SetTransport(std::shared_ptr<Transport> transport);
    std::shared_ptr<Transport> GetTransport();

    uint8_t GetDistrict() const;
    void SetDistrict(uint8_t district);
    void SetIgnoreErrorFlag(bool v);

    void SetCodecEncrypter(bool dec, int method, const std::string& key, uint64_t iv);
    void SetLastSeqNo(uint32_t seqno);

    PacketQueue*  IncomingQueue();
    ErrorQueue*   Errors();

    // register/unregister message callback
    void RegisterMessageCallback(MsgType command, MessageCallback callback);
    void UnRegisterMessageCallback(MsgType command);

	std::error_code Establish(asio::io_context& io_context, const std::string& address, ICodec* codec);

    // Run loop
    void Run();

    // Send message to specified server service
    void Send(ServiceType srv, MsgType command, const google::protobuf::Message* msg);

    // Send message and wait specified reply
    void Call(ServiceType srv, MsgType command, const google::protobuf::Message* msg, MessageCallback cb);

    // Like `Call`, but not waiting this reply
    void AsyncCall(ServiceType srv, MsgType command, const google::protobuf::Message* msg, MessageCallback cb);

    // Waiting message reply
    bool IsWaitingReply() const;

    void SendHeartBeat();

    void Start(bool keepalive);

    void Shutdown();
    void Close();

private:
    void Execute(PacketPtr packet);
    void HandlerError(const Error& err);
    void ReapReply(const std::error_code& ec);
    void OnKeepAlive(const std::error_code& ec);
    void OnRpcDone(PacketPtr packet);
    void DumpPacketLog(const std::string& custom, PacketPtr packet);

private:
    struct InternalImpl;
    std::unique_ptr<InternalImpl> impl_;
};

} // namespace net
