﻿// Copyright © 2016-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.


#include "Packet.h"
#include <assert.h>
#include <google/protobuf/message.h>

namespace net {


PacketPtr Packet::create()
{
    return std::make_shared<Packet>();
}

////////////////////////////////////////////////////////////////////////

Packet::Packet()
{
}

Packet::~Packet()
{
}

uint32_t Packet::GetError() const
{
    if ((flags & PacketFlagError) != 0)
    {
        return status;
    }
    return 0;
}

bool Packet::ParseMessage(google::protobuf::Message* msg)
{
    if (!msg->ParseFromArray(buf_.data(), buf_.size()))
    {
        return false;
    }
    read_ += buf_.size();
    return true;
}

void Packet::Read(void* data, size_t size)
{
    assert(AvailableReadBytes() >= (int)size);
    memcpy(data, &buf_[0] + read_, size);
    read_ += size;
}

void Packet::Append(const void* data, size_t size)
{
    size_t n = buf_.size();
    buf_.resize(n + size);
    memcpy(&buf_[n], (const char*)data, size);
}

} // namespace net
