// Copyright 2015-present Beyondtech, Inc. All Rights Reserved.
//
// http://www.3bodygame.com
//
// Any redistribution or reproduction of part or all of the contents in any form 
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially 
// exploit the content. Nor may you transmit it or store it in any other website or 
// other form of electronic retrieval system.

#pragma once

#include <string>
#include <memory>
#include <deque>
#include <system_error>

namespace net {

// error code between this range should make app restart
enum
{
    kRestartErrMin = 100000,
    kRestartErrMax = 100050,
};

struct Error
{
    Error(const std::string& desciption, const std::error_code& code)
        : desc(desciption), ec(code)
    {}

    std::error_code ec;     // error code
    std::string     desc;   // description
};

typedef std::deque<Error> ErrorQueue;

} // namespace net
