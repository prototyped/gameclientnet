﻿// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include <random>
#include "Codec.h"

class Buffer;

namespace net {

class Packet;

// wire format of a V1 packet header
//       ------------------------------------------------
// field |ver | len | cmd | flags | seq | node | status |
//       ------------------------------------------------
// bytes |  1 |  3  |  2  |  2    |  2  |   2  |   4    |
//       ------------------------------------------------
class CodecV1 : public ICodec
{
    enum 
    {
        CodecV1HeaderBytes = 16,
        CodecV1ProtocolVersion = 1,
        CodecV1MaxPayload = 1024 * 1024,
        CodecV1CompressThreshold = 1024,
        VersionBitsMask = 0x0FFFFFFF,
        VersionBitsShift = 28,
    };
public:
    CodecV1();
    ~CodecV1();

    CodecV1(const CodecV1&) = delete;
    CodecV1& operator=(const CodecV1&) = delete;

    int HeaderBytes() override { return CodecV1HeaderBytes; }

    int Decode(const Buffer& buffer, Packet* pkt, int* readLen);
    int Encode(Packet* packet, Buffer* buf);

    void SetCrypter(bool dec, ICrypter* crypter);
    ICrypter* GetCrypter(bool);

private:
    Compression     compress_;
    ICrypter*       encrypter_ = nullptr;
    ICrypter*       decrypter_ = nullptr;
};

} // namespace net
