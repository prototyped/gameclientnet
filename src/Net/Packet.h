﻿// Copyright © 2016-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

#include <cstdint>
#include <vector>
#include <type_traits>
#include "Define.h"

// forward declare
namespace google {
    namespace protobuf {
        class Message;
    }
}

namespace net {

// A Packet object represents an application message
class Packet 
{
public:
    static PacketPtr create();

public:
    uint16_t    command = 0;        // command message id
    uint16_t    flags = 0;          // option flags
    uint16_t    sequence = 0;       // sequence number
    uint16_t    node = 0;           // reference
    uint32_t    status = 0;         // error no.

public:
    explicit Packet();
    ~Packet();

    Packet(const Packet&) = delete;
    Packet& operator=(const Packet&) = delete;

    uint32_t Packet::GetError() const;

    int AvailableReadBytes() const { return (int)buf_.size() - read_; }

    void Read(void* data, size_t size);

    template <typename T>
    typename std::enable_if<std::is_pod<T>::value, T>::type
        Read()
    {
        T pod = T();
        Read(&pod, sizeof(T));
        return pod;
    }

    bool ParseMessage(google::protobuf::Message* msg);

    template <typename T>
    T* ParseMessage()
    {
        T* pMsg = new T();
        if (!ParseMessage(pMsg))
        {
            delete pMsg;
            pMsg = nullptr;
        }
        return pMsg;
    }

    void Append(const void* data, size_t size);

    void SerializeTo(std::vector<char>& buf)
    {
        buf.swap(buf);
    }

private:
    std::vector<char> buf_;
    int read_ = 0;
};


} // namespace net
