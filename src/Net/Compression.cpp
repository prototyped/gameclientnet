// Copyright 2016-present Beyondtech Inc. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form 
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially 
// exploit the content. Nor may you transmit it or store it in any other website or 
// other form of electronic retrieval system.

#include "Compression.h"
#include <zlib.h>
#include "Utility/Conv.h"
#include "Utility/ScopeGuard.h"

namespace net {
    
enum 
{
    DEFAULT_BUFFER_SIZE = 2048,
};

static BufferPtr doDeflate(z_stream* stream, std::vector<uint8_t>& buf, ByteRange data);
static BufferPtr doInflate(z_stream* stream, std::vector<uint8_t>& buf, ByteRange data);

Compression::Compression(int level /* =COMPRESSION_LEVEL_DEFAULT */)
{
    switch (level)
    {
    case COMPRESSION_LEVEL_FASTEST:
        level_ = Z_BEST_SPEED;
        break;
    case COMPRESSION_LEVEL_DEFAULT:
        level_ = Z_DEFAULT_COMPRESSION;
        break;
    case COMPRESSION_LEVEL_BEST:
        level_ = Z_BEST_COMPRESSION;
        break;
    default:
        throw std::invalid_argument(to<std::string>(
            "Compression: invalid level: ", level));
    }
    buffer_.resize(DEFAULT_BUFFER_SIZE);
}

Compression::~Compression()
{
}


BufferPtr Compression::Compress(ByteRange data)
{
    if (data.size() == 0)
    {
        return Buffer::create();
    }

    z_stream stream = {};
    int rc = deflateInit(&stream, level_);
    if (rc != Z_OK)
    {
        throw std::runtime_error(to<std::string>(
            "Compression: deflateInit error: ", rc, ": ", zError(rc)));
    }
    SCOPE_EXIT
    {
        int rc = deflateEnd(&stream);
        CHECK(rc == Z_OK) << zError(rc);
    };
    return doDeflate(&stream, buffer_, data);
}

BufferPtr Compression::UnCompress(ByteRange data)
{
    if (data.size() == 0)
    {
        return Buffer::create();
    }
    z_stream stream = {};
    int rc = inflateInit(&stream);
    if (rc != Z_OK)
    {
        throw std::runtime_error(to<std::string>(
            "UnCompress: inflateInit error: ", rc, ": ", zError(rc)));
    }
    SCOPE_EXIT 
    {
        int rc = inflateEnd(&stream);
        CHECK(rc == Z_OK) << zError(rc);
    };
    return doInflate(&stream, buffer_, data);
}


BufferPtr doDeflate(z_stream* stream, std::vector<uint8_t>& buf, ByteRange range)
{
    auto outbuf = Buffer::create();
    stream->next_in = (Bytef*)range.data();
    stream->avail_in = (unsigned int)range.size();
    stream->next_out = buf.data();
    stream->avail_out = buf.size();
    for (;;)
    {
        int rc = deflate(stream, Z_NO_FLUSH);
        if (rc != Z_OK)
        {
            throw std::runtime_error(to<std::string>(
                "doDeflate: deflate error: ", rc, ": ", zError(rc)));
        }
        if (stream->avail_out == 0)
        {
            ByteRange r(buf.data(), buf.size());
            outbuf->append(r);
            stream->next_out = buf.data();
            stream->avail_out = buf.size();
        }
        if (stream->avail_in == 0)
        {
            ByteRange r(buf.data(), buf.size() - stream->avail_out);
            outbuf->append(r);
            stream->next_out = buf.data();
            stream->avail_out = buf.size();
            break;
        }
    }
    if (stream->next_out)
    {
        int rc;
        do {
            rc = deflate(stream, Z_FINISH);
            if (rc != Z_OK && rc != Z_STREAM_END)
            {
                throw std::runtime_error(to<std::string>(
                    "Compress: deflate flush error: ", rc, ": ", zError(rc)));
            }
            ByteRange r(buf.data(), buf.size() - stream->avail_out);
            outbuf->append(r);
            stream->next_out = buf.data();
            stream->avail_out = buf.size();
        } while (rc != Z_STREAM_END);
    }
    return outbuf;
}

BufferPtr doInflate(z_stream* stream, std::vector<uint8_t>& buf, ByteRange range)
{
    auto outbuf = Buffer::create();
    stream->next_in = (Bytef*)range.data();
    stream->avail_in = (unsigned int)range.size();
    stream->next_out = buf.data();
    stream->avail_out = buf.size();
    for (;;)
    {
        int rc = inflate(stream, Z_NO_FLUSH);
        if (rc != Z_OK && rc != Z_STREAM_END)
        {
            throw std::runtime_error(to<std::string>(
                "doInflate: inflate error: ", rc, ": ", zError(rc)));
        }
        if (rc == Z_STREAM_END)
        {
            ByteRange r(buf.data(), buf.size() - stream->avail_out);
            outbuf->append(r);
            break;
        }
        if (stream->avail_out == 0)
        {
            ByteRange r(buf.data(), buf.size());
            outbuf->append(r);
            stream->next_out = buf.data();
            stream->avail_out = buf.size();
        }
        if (stream->avail_in == 0)
        {
            ByteRange r(buf.data(), buf.size() - stream->avail_out);
            outbuf->append(r);
            stream->next_out = buf.data();
            stream->avail_out = buf.size();
        }
    }
    return outbuf;
}

} // namespace net