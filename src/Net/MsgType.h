﻿// Copyright © 2017-present ichenq@outlook.com. All Rights Reserved.
//
// Any redistribution or reproduction of part or all of the contents in any form
// is prohibited.
//
// You may not, except with our express written permission, distribute or commercially
// exploit the content. Nor may you transmit it or store it in any other website or
// other form of electronic retrieval system.

#pragma once

enum MsgType : uint16_t
{
    SM_DISCONNECT_NOTIFY        = 3001, //
    CM_HEARTBEAT                = 3002, // 心跳
    SM_HEARTBEAT_STATUS         = 3003, // 心跳返回
    CM_CLIENT_HANDSHAKE         = 3004, // client hello
    SM_CLIENT_HANDSHAKE_STATUS  = 3005, // server hello
    SM_KEY_EXCHANGE_NOTIFY      = 3006, // 交换密钥
    SM_NOTIFY_ERROR             = 3007, // 错误码通知
    CM_LOGIN                    = 3101, // 登陆获取网关
    SM_LOGIN_STATUS             = 3102, // 登陆返回

};
