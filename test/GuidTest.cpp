#include "Utility/GUID.h"
#include <gtest/gtest.h>
#include <map>

using namespace std;

TEST(GUID, Create)
{
    std::string uuid = GenerateGUID();
    EXPECT_TRUE(IsValidGUID(uuid));

    std::map<std::string, bool> exist;
    for (int i = 0; i < 1000; i++)
    {
        uuid = GenerateGUID();
        EXPECT_EQ(exist.count(uuid), 0);
        exist[uuid] = true;
    }
}

