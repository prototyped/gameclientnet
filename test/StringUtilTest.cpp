#include "Utility/StringUtil.h"
#include "Utility/ScopeGuard.h"
#include <gtest/gtest.h>
#include <cinttypes>
#include <cstdarg>
#include <random>
#include <memory>

using namespace std;

void TestStringSplit()
{
    std::string s;
    std::vector<StringPiece> parts;

    parts = Split("a,b,c", ",", false);
    EXPECT_EQ(parts.size(), 3);
    EXPECT_EQ(parts[0], "a");
    EXPECT_EQ(parts[1], "b");
    EXPECT_EQ(parts[2], "c");
    parts.clear();

    parts = Split(StringPiece("a,b,c"), ",", false);
    EXPECT_EQ(parts.size(), 3);
    EXPECT_EQ(parts[0], "a");
    EXPECT_EQ(parts[1], "b");
    EXPECT_EQ(parts[2], "c");
    parts.clear();

    s = ("a,b,c");
    parts = Split(s, ",", false);
    EXPECT_EQ(parts.size(), 3);
    EXPECT_EQ(parts[0], "a");
    EXPECT_EQ(parts[1], "b");
    EXPECT_EQ(parts[2], "c");
    parts.clear();

    parts = Split("a,,c", ",", false);
    EXPECT_EQ(parts.size(), 3);
    EXPECT_EQ(parts[0], "a");
    EXPECT_EQ(parts[1], "");
    EXPECT_EQ(parts[2], "c");
    parts.clear();

    s = ("a,,c");
    parts = Split(s, ",", false);
    EXPECT_EQ(parts.size(), 3);
    EXPECT_EQ(parts[0], "a");
    EXPECT_EQ(parts[1], "");
    EXPECT_EQ(parts[2], "c");
    parts.clear();

    parts = Split("a,,c", ",", true);
    EXPECT_EQ(parts.size(), 2);
    EXPECT_EQ(parts[0], "a");
    EXPECT_EQ(parts[1], "c");
    parts.clear();

    s = ("a,,c");
    parts = Split(s, ",", true);
    EXPECT_EQ(parts.size(), 2);
    EXPECT_EQ(parts[0], "a");
    EXPECT_EQ(parts[1], "c");
    parts.clear();

    s = (",,a,,c,,,");
    parts = Split(s, ",", true);
    EXPECT_EQ(parts.size(), 2);
    EXPECT_EQ(parts[0], "a");
    EXPECT_EQ(parts[1], "c");
    parts.clear();

    // test splits that with multi-line delimiter
    parts = Split("dabcabkdbkab", "ab", true);
    EXPECT_EQ(parts.size(), 3);
    EXPECT_EQ(parts[0], "d");
    EXPECT_EQ(parts[1], "c");
    EXPECT_EQ(parts[2], "kdbk");
    parts.clear();

    // test last part is shorter than the delimiter
    parts = Split("abcd", "bc", true);
    EXPECT_EQ(parts.size(), 2);
    EXPECT_EQ(parts[0], "a");
    EXPECT_EQ(parts[1], "d");
    parts.clear();

    string orig = "ab2342asdfv~~!";
    parts = Split(orig, "", true);
    EXPECT_EQ(parts.size(), 1);
    EXPECT_EQ(parts[0], orig);
    parts.clear();

    s = "452x;o38asfsajsdlfdf.j";
    parts = Split(s, "asfds", true);
    EXPECT_EQ(parts.size(), 1);
    EXPECT_EQ(parts[0], s);
    parts.clear();

    parts = Split("a", "", true);
    EXPECT_EQ(parts.size(), 1);
    EXPECT_EQ(parts[0], "a");
    parts.clear();

    parts = Split("", "a", false);
    EXPECT_EQ(parts.size(), 1);
    EXPECT_EQ(parts[0], "");
    parts.clear();

    parts = Split("", "a", true);
    EXPECT_EQ(parts.size(), 0);
    parts.clear();

    parts = Split(StringPiece(), "a", true);
    EXPECT_EQ(parts.size(), 0);
    parts.clear();

    parts = Split("abcdefg", "a", true);
    EXPECT_EQ(parts.size(), 1);
    EXPECT_EQ(parts[0], "bcdefg");
    parts.clear();

    orig = "All, , your base, are , , belong to us";
    parts = Split(orig, ", ", true);
    EXPECT_EQ(parts.size(), 4);
    EXPECT_EQ(parts[0], "All");
    EXPECT_EQ(parts[1], "your base");
    EXPECT_EQ(parts[2], "are ");
    EXPECT_EQ(parts[3], "belong to us");
    parts.clear();
    parts = Split(orig, ", ", false);
    EXPECT_EQ(parts.size(), 6);
    EXPECT_EQ(parts[0], "All");
    EXPECT_EQ(parts[1], "");
    EXPECT_EQ(parts[2], "your base");
    EXPECT_EQ(parts[3], "are ");
    EXPECT_EQ(parts[4], "");
    EXPECT_EQ(parts[5], "belong to us");
    parts.clear();

    orig = ", Facebook, rul,es!, ";
    parts = Split(orig, ", ", true);
    EXPECT_EQ(parts.size(), 2);
    EXPECT_EQ(parts[0], "Facebook");
    EXPECT_EQ(parts[1], "rul,es!");
    parts.clear();
    parts = Split(orig, ", ", false);
    EXPECT_EQ(parts.size(), 4);
    EXPECT_EQ(parts[0], "");
    EXPECT_EQ(parts[1], "Facebook");
    EXPECT_EQ(parts[2], "rul,es!");
    EXPECT_EQ(parts[3], "");
}


TEST(stringPrintf, BasicTest)
{
    EXPECT_EQ("abc", stringPrintf("%s", "abc"));
    EXPECT_EQ("abc", stringPrintf("%sbc", "a"));
    EXPECT_EQ("abc", stringPrintf("a%sc", "b"));
    EXPECT_EQ("abc", stringPrintf("ab%s", "c"));

    EXPECT_EQ("abc", stringPrintf("abc"));
}


TEST(stringPrintf, NumericFormats)
{
    EXPECT_EQ("12", stringPrintf("%d", 12));
    EXPECT_EQ("5000000000", stringPrintf("%" PRIu64, 5000000000UL));
    EXPECT_EQ("5000000000", stringPrintf("%" PRId64, 5000000000LL));
    EXPECT_EQ("-5000000000", stringPrintf("%" PRId64, -5000000000L));
    EXPECT_EQ("-1", stringPrintf("%d", 0xffffffff));
    EXPECT_EQ("-1", stringPrintf("%" PRId64, 0xffffffffffffffff));
    EXPECT_EQ("-1", stringPrintf("%" PRId64, 0xffffffffffffffffUL));

    EXPECT_EQ("7.7", stringPrintf("%1.1f", 7.7));
    EXPECT_EQ("7.7", stringPrintf("%1.1lf", 7.7));

	// not so accurate
    // EXPECT_EQ("7.70000000000000018", stringPrintf("%.17f", 7.7));
    // EXPECT_EQ("7.70000000000000018", stringPrintf("%.17lf", 7.7));
}

TEST(stringPrintf, Appending)
{
    string s;
    stringAppendF(&s, "a%s", "b");
    stringAppendF(&s, "%c", 'c');
    EXPECT_EQ(s, "abc");
    stringAppendF(&s, " %d", 123);
    EXPECT_EQ(s, "abc 123");
}

static void vprintfCheck(const char* expected, const char* fmt, ...)
{
    va_list apOrig;
    va_start(apOrig, fmt);
    SCOPE_EXIT
    {
        va_end(apOrig);
    };
    va_list ap;
    va_copy(ap, apOrig);
    SCOPE_EXIT
    {
        va_end(ap);
    };

    // Check both APIs for calling stringVPrintf()
    EXPECT_EQ(expected, stringVPrintf(fmt, ap));
    va_end(ap);
    va_copy(ap, apOrig);

    std::string out;
    stringVPrintf(&out, fmt, ap);
    va_end(ap);
    va_copy(ap, apOrig);
    EXPECT_EQ(expected, out);

    // Check stringVAppendf() as well
    std::string prefix = "foobar";
    out = prefix;
    EXPECT_EQ(prefix + expected, stringVAppendF(&out, fmt, ap));
    va_end(ap);
    va_copy(ap, apOrig);
}

static void vprintfError(const char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    SCOPE_EXIT
    {
        va_end(ap);
    };
}

TEST(stringPrintf, VPrintf)
{
    vprintfCheck("foo", "%s", "foo");
    vprintfCheck("long string requiring reallocation 1 2 3 0x12345678",
        "%s %s %d %d %d %#x",
        "long string", "requiring reallocation", 1, 2, 3, 0x12345678);
    vprintfError("bogus%", "foo");
}

TEST(stringPrintf, VariousSizes)
{
    // Test a wide variety of output sizes, making sure to cross the
    // vsnprintf buffer boundary implementation detail.
    for (int i = 0; i < 4096; ++i) {
        string expected(i + 1, 'a');
        expected = "X" + expected + "X";
        string result = stringPrintf("%s", expected.c_str());
        EXPECT_EQ(expected.size(), result.size());
        EXPECT_EQ(expected, result);
    }

    EXPECT_EQ("abc12345678910111213141516171819202122232425xyz",
        stringPrintf("abc%d%d%d%d%d%d%d%d%d%d%d%d%d%d"
        "%d%d%d%d%d%d%d%d%d%d%dxyz",
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
        17, 18, 19, 20, 21, 22, 23, 24, 25));
}

TEST(stringPrintf, oldStringPrintfTests)
{
    EXPECT_EQ(string("a/b/c/d"),
        stringPrintf("%s/%s/%s/%s", "a", "b", "c", "d"));

    EXPECT_EQ(string("    5    10"),
        stringPrintf("%5d %5d", 5, 10));

    // check printing w/ a big buffer
    for (int size = (1 << 8); size <= (1 << 15); size <<= 1) {
        string a(size, 'z');
        string b = stringPrintf("%s", a.c_str());
        EXPECT_EQ(a.size(), b.size());
    }
}

TEST(stringPrintf, oldStringAppendf)
{
    string s = "hello";
    stringAppendF(&s, "%s/%s/%s/%s", "a", "b", "c", "d");
    EXPECT_EQ(string("helloa/b/c/d"), s);
}


TEST(StringPrintfTest, Empty) {
#if 0
    // gcc 2.95.3, gcc 4.1.0, and gcc 4.2.2 all warn about this:
    // warning: zero-length printf format string.
    // so we do not allow them in google3.
    EXPECT_EQ("", stringPrintf(""));
#endif
    EXPECT_EQ("", stringPrintf("%s", string().c_str()));
    EXPECT_EQ("", stringPrintf("%s", ""));
}

TEST(StringPrintfTest, Misc) {
    // MSVC and mingw does not support $ format specifier.
#if !defined(_MSC_VER) && !defined(__MINGW32__)
    EXPECT_EQ("123hello w", stringPrintf("%3$d%2$s %1$c", 'w', "hello", 123));
#endif  // !_MSC_VER
}

TEST(StringAppendFTest, Empty) {
    string value("Hello");
    const char* empty = "";
    stringAppendF(&value, "%s", empty);
    EXPECT_EQ("Hello", value);
}

TEST(StringAppendFTest, EmptyString) {
    string value("Hello");
    stringAppendF(&value, "%s", "");
    EXPECT_EQ("Hello", value);
}

TEST(StringAppendFTest, String) {
    string value("Hello");
    stringAppendF(&value, " %s", "World");
    EXPECT_EQ("Hello World", value);
}

TEST(StringAppendFTest, Int) {
    string value("Hello");
    stringAppendF(&value, " %d", 123);
    EXPECT_EQ("Hello 123", value);
}

TEST(StringPrintfTest, Multibyte) {
    // If we are in multibyte mode and feed invalid multibyte sequence,
    // stringPrintf should return an empty string instead of running
    // out of memory while trying to determine destination buffer size.
    // see b/4194543.

    char* old_locale = setlocale(LC_CTYPE, NULL);
    // Push locale with multibyte mode
    setlocale(LC_CTYPE, "en_US.utf8");

    const char kInvalidCodePoint[] = "\375\067s";
    string value = stringPrintf("%.*s", 3, kInvalidCodePoint);

    // In some versions of glibc (e.g. eglibc-2.11.1, aka GRTEv2), snprintf
    // returns error given an invalid codepoint. Other versions
    // (e.g. eglibc-2.15, aka pre-GRTEv3) emit the codepoint verbatim.
    // We test that the output is one of the above.
    EXPECT_TRUE(value.empty() || value == kInvalidCodePoint);

    // Repeat with longer string, to make sure that the dynamically
    // allocated path in StringAppendV is handled correctly.
    int n = 2048;
    char* buf = new char[n + 1];
    memset(buf, ' ', n - 3);
    memcpy(buf + n - 3, kInvalidCodePoint, 4);
    value = stringPrintf("%.*s", n, buf);
    // See GRTEv2 vs. GRTEv3 comment above.
    EXPECT_TRUE(value.empty() || value == buf);
    delete[] buf;

    setlocale(LC_CTYPE, old_locale);
}

TEST(StringPrintfTest, NoMultibyte) {
    // No multibyte handling, but the string contains funny chars.
    char* old_locale = setlocale(LC_CTYPE, NULL);
    setlocale(LC_CTYPE, "POSIX");
    string value = stringPrintf("%.*s", 3, "\375\067s");
    setlocale(LC_CTYPE, old_locale);
    EXPECT_EQ("\375\067s", value);
}

TEST(StringPrintfTest, DontOverwriteErrno) {
    // Check that errno isn't overwritten unless we're printing
    // something significantly larger than what people are normally
    // printing in their badly written PLOG() statements.
    errno = ECHILD;
    string value = stringPrintf("Hello, %s!", "World");
    EXPECT_EQ(ECHILD, errno);
}

TEST(StringPrintfTest, LargeBuf) {
    // Check that the large buffer is handled correctly.
    int n = 2048;
    char* buf = new char[n + 1];
    memset(buf, ' ', n);
    buf[n] = 0;
    string value = stringPrintf("%s", buf);
    EXPECT_EQ(buf, value);
    delete[] buf;
}

TEST(Escape, cEscape)
{
    EXPECT_EQ("hello world", cEscape("hello world"));
    EXPECT_EQ("hello \\\\world\\\" goodbye",
        cEscape("hello \\world\" goodbye"));
    EXPECT_EQ("hello\\nworld", cEscape("hello\nworld"));
    EXPECT_EQ("hello\\377\\376", cEscape("hello\xff\xfe"));
}

TEST(Escape, cUnescape) {
    EXPECT_EQ("hello world", cUnescape("hello world"));
    EXPECT_EQ("hello \\world\" goodbye",
        cUnescape("hello \\\\world\\\" goodbye"));
    EXPECT_EQ("hello\nworld", cUnescape("hello\\nworld"));
    EXPECT_EQ("hello\nworld", cUnescape("hello\\012world"));
    EXPECT_EQ("hello\nworld", cUnescape("hello\\x0aworld"));
    EXPECT_EQ("hello\xff\xfe", cUnescape("hello\\377\\376"));
    EXPECT_EQ("hello\xff\xfe", cUnescape("hello\\xff\\xfe"));

    EXPECT_THROW({ cUnescape("hello\\"); },
        std::invalid_argument);
    EXPECT_THROW({ cUnescape("hello\\x"); },
        std::invalid_argument);
    EXPECT_THROW({ cUnescape("hello\\q"); },
        std::invalid_argument);
}

TEST(Escape, uriEscape)
{
    EXPECT_EQ("hello%2c%20%2fworld", uriEscape("hello, /world"));
    EXPECT_EQ("hello%2c%20/world", uriEscape("hello, /world", UriEscapeMode::PATH));
    EXPECT_EQ("hello%2c+%2fworld", uriEscape("hello, /world", UriEscapeMode::QUERY));
    EXPECT_EQ(
        "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_.~",
        uriEscape(
        "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_.~")
        );
}

TEST(Escape, uriUnescape)
{
    EXPECT_EQ("hello, /world", uriUnescape("hello, /world"));
    EXPECT_EQ("hello, /world", uriUnescape("hello%2c%20%2fworld"));
    EXPECT_EQ("hello,+/world", uriUnescape("hello%2c+%2fworld"));
    EXPECT_EQ("hello, /world", uriUnescape("hello%2c+%2fworld", UriEscapeMode::QUERY));

    EXPECT_EQ("hello/", uriUnescape("hello%2f"));
    EXPECT_EQ("hello/", uriUnescape("hello%2F"));
    EXPECT_THROW({ uriUnescape("hello%"); }, std::invalid_argument);
    EXPECT_THROW({ uriUnescape("hello%2"); }, std::invalid_argument);
    EXPECT_THROW({ uriUnescape("hello%2g"); }, std::invalid_argument);
}


namespace {
    void expectPrintable(StringPiece s)
    {
        for (char c : s) {
            EXPECT_LE(32, c);
            EXPECT_GE(127, c);
        }
    }
}  // namespace

TEST(Escape, uriEscapeAllCombinations)
{
    char c[3];
    c[2] = '\0';
    StringPiece in(c, 2);
    string tmp;
    string out;
    for (int i = 0; i < 256; ++i) {
        c[0] = i;
        for (int j = 0; j < 256; ++j) {
            c[1] = j;
            tmp.clear();
            out.clear();
            uriEscape(in, tmp);
            expectPrintable(tmp);
            uriUnescape(tmp, out);
            EXPECT_EQ(in, out);
        }
    }
}

namespace {
    bool isHex(int v)
    {
        return ((v >= '0' && v <= '9') ||
            (v >= 'A' && v <= 'F') ||
            (v >= 'a' && v <= 'f'));
    }
}  // namespace


TEST(Escape, uriUnescapePercentDecoding)
{
    char c[4] = { '%', '\0', '\0', '\0' };
    StringPiece in(c, 3);
    std::string out;
    unsigned int expected = 0;
    for (int i = 0; i < 256; ++i) {
        c[1] = i;
        for (int j = 0; j < 256; ++j) {
            c[2] = j;
            if (isHex(i) && isHex(j)) {
                out.clear();
                uriUnescape(in, out);
                EXPECT_EQ(1, out.size());
                EXPECT_EQ(1, sscanf(c + 1, "%x", &expected));
                unsigned char v = out[0];
                EXPECT_EQ(expected, v);
            }
            else {
                EXPECT_THROW({ uriUnescape(in, out); }, std::invalid_argument);
            }
        }
    }
}


namespace {

    double pow2(int exponent) {
        return double(int64_t(1) << exponent);
    }

}  // namespace
struct PrettyTestCase{
    std::string prettyString;
    double realValue;
    PrettyType prettyType;
};

PrettyTestCase prettyTestCases[] =
{
    { string("8.53e+07 s "), 85.3e6, PRETTY_TIME },
    { string("8.53e+07 s "), 85.3e6, PRETTY_TIME },
    { string("85.3 ms"), 85.3e-3, PRETTY_TIME },
    { string("85.3 us"), 85.3e-6, PRETTY_TIME },
    { string("85.3 ns"), 85.3e-9, PRETTY_TIME },
    { string("85.3 ps"), 85.3e-12, PRETTY_TIME },
    { string("8.53e-14 s "), 85.3e-15, PRETTY_TIME },

    { string("0 s "), 0, PRETTY_TIME },
    { string("1 s "), 1.0, PRETTY_TIME },
    { string("1 ms"), 1.0e-3, PRETTY_TIME },
    { string("1 us"), 1.0e-6, PRETTY_TIME },
    { string("1 ns"), 1.0e-9, PRETTY_TIME },
    { string("1 ps"), 1.0e-12, PRETTY_TIME },

    // check bytes printing
    { string("853 B "), 853., PRETTY_BYTES },
    { string("833 kB"), 853.e3, PRETTY_BYTES },
    { string("813.5 MB"), 853.e6, PRETTY_BYTES },
    { string("7.944 GB"), 8.53e9, PRETTY_BYTES },
    { string("794.4 GB"), 853.e9, PRETTY_BYTES },
    { string("775.8 TB"), 853.e12, PRETTY_BYTES },

    { string("0 B "), 0, PRETTY_BYTES },
    { string("1 B "), pow2(0), PRETTY_BYTES },
    { string("1 kB"), pow2(10), PRETTY_BYTES },
    { string("1 MB"), pow2(20), PRETTY_BYTES },
    { string("1 GB"), pow2(30), PRETTY_BYTES },
    { string("1 TB"), pow2(40), PRETTY_BYTES },

    { string("853 B  "), 853., PRETTY_BYTES_IEC },
    { string("833 KiB"), 853.e3, PRETTY_BYTES_IEC },
    { string("813.5 MiB"), 853.e6, PRETTY_BYTES_IEC },
    { string("7.944 GiB"), 8.53e9, PRETTY_BYTES_IEC },
    { string("794.4 GiB"), 853.e9, PRETTY_BYTES_IEC },
    { string("775.8 TiB"), 853.e12, PRETTY_BYTES_IEC },

    { string("0 B  "), 0, PRETTY_BYTES_IEC },
    { string("1 B  "), pow2(0), PRETTY_BYTES_IEC },
    { string("1 KiB"), pow2(10), PRETTY_BYTES_IEC },
    { string("1 MiB"), pow2(20), PRETTY_BYTES_IEC },
    { string("1 GiB"), pow2(30), PRETTY_BYTES_IEC },
    { string("1 TiB"), pow2(40), PRETTY_BYTES_IEC },

    // check bytes metric printing
    { string("853 B "), 853., PRETTY_BYTES_METRIC },
    { string("853 kB"), 853.e3, PRETTY_BYTES_METRIC },
    { string("853 MB"), 853.e6, PRETTY_BYTES_METRIC },
    { string("8.53 GB"), 8.53e9, PRETTY_BYTES_METRIC },
    { string("853 GB"), 853.e9, PRETTY_BYTES_METRIC },
    { string("853 TB"), 853.e12, PRETTY_BYTES_METRIC },

    { string("0 B "), 0, PRETTY_BYTES_METRIC },
    { string("1 B "), 1.0, PRETTY_BYTES_METRIC },
    { string("1 kB"), 1.0e+3, PRETTY_BYTES_METRIC },
    { string("1 MB"), 1.0e+6, PRETTY_BYTES_METRIC },

    { string("1 GB"), 1.0e+9, PRETTY_BYTES_METRIC },
    { string("1 TB"), 1.0e+12, PRETTY_BYTES_METRIC },

    // check metric-units (powers of 1000) printing
    { string("853  "), 853., PRETTY_UNITS_METRIC },
    { string("853 k"), 853.e3, PRETTY_UNITS_METRIC },
    { string("853 M"), 853.e6, PRETTY_UNITS_METRIC },
    { string("8.53 bil"), 8.53e9, PRETTY_UNITS_METRIC },
    { string("853 bil"), 853.e9, PRETTY_UNITS_METRIC },
    { string("853 tril"), 853.e12, PRETTY_UNITS_METRIC },

    // check binary-units (powers of 1024) printing
    { string("0  "), 0, PRETTY_UNITS_BINARY },
    { string("1  "), pow2(0), PRETTY_UNITS_BINARY },
    { string("1 k"), pow2(10), PRETTY_UNITS_BINARY },
    { string("1 M"), pow2(20), PRETTY_UNITS_BINARY },
    { string("1 G"), pow2(30), PRETTY_UNITS_BINARY },
    { string("1 T"), pow2(40), PRETTY_UNITS_BINARY },

    { string("1023  "), pow2(10) - 1, PRETTY_UNITS_BINARY },
    { string("1024 k"), pow2(20) - 1, PRETTY_UNITS_BINARY },
    { string("1024 M"), pow2(30) - 1, PRETTY_UNITS_BINARY },
    { string("1024 G"), pow2(40) - 1, PRETTY_UNITS_BINARY },

    { string("0   "), 0, PRETTY_UNITS_BINARY_IEC },
    { string("1   "), pow2(0), PRETTY_UNITS_BINARY_IEC },
    { string("1 Ki"), pow2(10), PRETTY_UNITS_BINARY_IEC },
    { string("1 Mi"), pow2(20), PRETTY_UNITS_BINARY_IEC },
    { string("1 Gi"), pow2(30), PRETTY_UNITS_BINARY_IEC },
    { string("1 Ti"), pow2(40), PRETTY_UNITS_BINARY_IEC },

    { string("1023   "), pow2(10) - 1, PRETTY_UNITS_BINARY_IEC },
    { string("1024 Ki"), pow2(20) - 1, PRETTY_UNITS_BINARY_IEC },
    { string("1024 Mi"), pow2(30) - 1, PRETTY_UNITS_BINARY_IEC },
    { string("1024 Gi"), pow2(40) - 1, PRETTY_UNITS_BINARY_IEC },

    //check border SI cases

    { string("1 Y"), 1e24, PRETTY_SI },
    { string("10 Y"), 1e25, PRETTY_SI },
    { string("1 y"), 1e-24, PRETTY_SI },
    { string("10 y"), 1e-23, PRETTY_SI },

    // check that negative values work
    { string("-85.3 s "), -85.3, PRETTY_TIME },
    { string("-85.3 ms"), -85.3e-3, PRETTY_TIME },
    { string("-85.3 us"), -85.3e-6, PRETTY_TIME },
    { string("-85.3 ns"), -85.3e-9, PRETTY_TIME },
    // end of test
    { string("endoftest"), 0, PRETTY_NUM_TYPES }
};


TEST(PrettyPrint, Basic)
{
    for (int i = 0; prettyTestCases[i].prettyType != PRETTY_NUM_TYPES; ++i){
        const PrettyTestCase& prettyTest = prettyTestCases[i];
        EXPECT_EQ(prettyTest.prettyString,
            prettyPrint(prettyTest.realValue, prettyTest.prettyType));
    }
}

TEST(PrettyToDouble, Basic)
{
    // check manually created tests
    for (int i = 0; prettyTestCases[i].prettyType != PRETTY_NUM_TYPES; ++i){
        PrettyTestCase testCase = prettyTestCases[i];
        PrettyType formatType = testCase.prettyType;
        double x = testCase.realValue;
        std::string testString = testCase.prettyString;
        double recoveredX;
        try{
            recoveredX = prettyToDouble(testString, formatType);
        }
        catch (std::range_error&){
            EXPECT_TRUE(false);
        }
        double relativeError = fabs(x) < 1e-5 ? (x - recoveredX) :
            (x - recoveredX) / x;
        EXPECT_NEAR(0, relativeError, 1e-3);
    }

    // checks for compatibility with prettyPrint over the whole parameter space
    for (int i = 0; i < PRETTY_NUM_TYPES; ++i){
        PrettyType formatType = static_cast<PrettyType>(i);
        for (double x = 1e-18; x < 1e40; x *= 1.9){
            bool addSpace = static_cast<PrettyType> (i) == PRETTY_SI;
            for (int it = 0; it < 2; ++it, addSpace = true){
                double recoveredX;
                try{
                    recoveredX = prettyToDouble(prettyPrint(x, formatType, addSpace),
                        formatType);
                }
                catch (std::range_error&){
                    EXPECT_TRUE(false);
                }
                double relativeError = (x - recoveredX) / x;
                EXPECT_NEAR(0, relativeError, 1e-3);
            }
        }
    }

    // check for incorrect values
    EXPECT_THROW(prettyToDouble("10Mx", PRETTY_SI), std::range_error);
    EXPECT_THROW(prettyToDouble("10 Mx", PRETTY_SI), std::range_error);
    EXPECT_THROW(prettyToDouble("10 M x", PRETTY_SI), std::range_error);

    StringPiece testString = "10Mx";
    EXPECT_DOUBLE_EQ(prettyToDouble(&testString, PRETTY_UNITS_METRIC), 10e6);
    EXPECT_EQ(testString, "x");
}

TEST(String, hexlify)
{
    string input1 = "0123";
    string output1;
    EXPECT_TRUE(hexlify(input1, output1));
    EXPECT_EQ(output1, "30313233");

    string input2 = "abcdefg";
    input2[1] = 0;
    input2[3] = (char)0xff;
    input2[5] = (char)0xb6;
    string output2;
    EXPECT_TRUE(hexlify(input2, output2));
    EXPECT_EQ(output2, "610063ff65b667");
}

TEST(String, unhexlify)
{
    string input1 = "30313233";
    string output1;
    EXPECT_TRUE(unhexlify(input1, output1));
    EXPECT_EQ(output1, "0123");

    string input2 = "610063ff65b667";
    string output2;
    EXPECT_TRUE(unhexlify(input2, output2));
    EXPECT_EQ(output2.size(), 7);
    EXPECT_EQ(output2[0], 'a');
    EXPECT_EQ(output2[1], 0);
    EXPECT_EQ(output2[2], 'c');
    EXPECT_EQ(output2[3] & 0xff, 0xff);
    EXPECT_EQ(output2[4], 'e');
    EXPECT_EQ(output2[5] & 0xff, 0xb6);
    EXPECT_EQ(output2[6], 'g');

    string input3 = "x";
    string output3;
    EXPECT_FALSE(unhexlify(input3, output3));

    string input4 = "xy";
    string output4;
    EXPECT_FALSE(unhexlify(input4, output4));
}

TEST(String, backslashify)
{
    EXPECT_EQ("abc", string("abc"));
    EXPECT_EQ("abc", backslashify(string("abc")));
    EXPECT_EQ("abc\\r", backslashify(string("abc\r")));
    EXPECT_EQ("abc\\x0d", backslashify(string("abc\r"), true));
    EXPECT_EQ("\\0\\0", backslashify(string(2, '\0')));
}

TEST(String, humanify)
{
    // Simple cases; output is obvious.
    EXPECT_EQ("abc", humanify(string("abc")));
    EXPECT_EQ("abc\\\\r", humanify(string("abc\\r")));
    EXPECT_EQ("0xff", humanify(string("\xff")));
    EXPECT_EQ("abc\\xff", humanify(string("abc\xff")));
    EXPECT_EQ("abc\\b", humanify(string("abc\b")));
    EXPECT_EQ("0x00", humanify(string(1, '\0')));
    EXPECT_EQ("0x0000", humanify(string(2, '\0')));


    // Mostly printable, so backslash!  80, 60, and 40% printable, respectively
    EXPECT_EQ("aaaa\\xff", humanify(string("aaaa\xff")));
    EXPECT_EQ("aaa\\xff\\xff", humanify(string("aaa\xff\xff")));
    EXPECT_EQ("aa\\xff\\xff\\xff", humanify(string("aa\xff\xff\xff")));

    // 20% printable, and the printable portion isn't the prefix; hexify!
    EXPECT_EQ("0xff61ffffff", humanify(string("\xff" "a\xff\xff\xff")));

    // Same as previous, except swap first two chars; prefix is
    // printable and within the threshold, so backslashify.
    EXPECT_EQ("a\\xff\\xff\\xff\\xff", humanify(string("a\xff\xff\xff\xff")));

    // Just too much unprintable; hex, despite prefix.
    EXPECT_EQ("0x61ffffffffff", humanify(string("a\xff\xff\xff\xff\xff")));
}


namespace {

    /**
    * Copy bytes from src to somewhere in the buffer referenced by dst. The
    * actual starting position of the copy will be the first address in the
    * destination buffer whose address mod 8 is equal to the src address mod 8.
    * The caller is responsible for ensuring that the destination buffer has
    * enough extra space to accommodate the shifted copy.
    */
    char* copyWithSameAlignment(char* dst, const char* src, size_t length)
    {
        const char* originalDst = dst;
        size_t dstOffset = size_t(dst) & 0x7;
        size_t srcOffset = size_t(src) & 0x7;
        while (dstOffset != srcOffset) {
            dst++;
            dstOffset++;
            dstOffset &= 0x7;
        }
        CHECK(dst <= originalDst + 7);
        CHECK((size_t(dst) & 0x7) == (size_t(src) & 0x7));
        memcpy(dst, src, length);
        return dst;
    }

    void testToLowerAscii(Range<const char*> src)
    {
        // Allocate extra space so we can make copies that start at the
        // same alignment (byte, word, quadword, etc) as the source buffer.
        std::unique_ptr<char> controlBuf(new char[src.size() + 7]);
        char* control = copyWithSameAlignment(controlBuf.get(), src.begin(), src.size());

        std::unique_ptr<char> testBuf(new char[src.size() + 7]);
        char* test = copyWithSameAlignment(testBuf.get(), src.begin(), src.size());

        for (size_t i = 0; i < src.size(); i++) {
            control[i] = tolower(control[i]);
        }
        toLowerAscii(test, src.size());
        for (size_t i = 0; i < src.size(); i++) {
            EXPECT_EQ(control[i], test[i]);
        }
    }

} // anon namespace

TEST(String, toLowerAsciiAligned)
{
    static const size_t kSize = 256;
    char input[kSize];
    for (size_t i = 0; i < kSize; i++) {
        input[i] = (char)(i & 0xff);
    }
    testToLowerAscii(Range<const char*>(input, kSize));
}

TEST(String, toLowerAsciiUnaligned)
{
    static const size_t kSize = 256;
    char input[kSize];
    for (size_t i = 0; i < kSize; i++) {
        input[i] = (char)(i & 0xff);
    }
    // Test input buffers of several lengths to exercise all the
    // cases: buffer at the start/middle/end of an aligned block, plus
    // buffers that span multiple aligned blocks.  The longest test input
    // is 3 unaligned bytes + 4 32-bit aligned bytes + 8 64-bit aligned
    // + 4 32-bit aligned + 3 unaligned = 22 bytes.
    for (size_t length = 1; length < 23; length++) {
        for (size_t offset = 0; offset + length <= kSize; offset++) {
            testToLowerAscii(Range<const char*>(input + offset, length));
        }
    }
}
