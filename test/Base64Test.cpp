#include "Utility/Base64.h"
#include <gtest/gtest.h>
#include "Utility/StringUtil.h"

using namespace std;

TEST(Base64, Encode)
{
	std::vector<std::pair<std::string, std::string>> pairs;
	pairs.push_back(std::make_pair(
		"hellokitty", 
		"aGVsbG9raXR0eQ=="));
	pairs.push_back(std::make_pair(
		"a quick brown fox jumps over a lazy dog", 
		"YSBxdWljayBicm93biBmb3gganVtcHMgb3ZlciBhIGxhenkgZG9n"));
	pairs.push_back(std::make_pair(
		"ciM2*Vp2v&rOWEDR$&PM7Q83uGnPcuDsY2lNMipWcDJ2JnJPV0VEUiQmUE03UTgzdUduUGN1RHM=", 
		"Y2lNMipWcDJ2JnJPV0VEUiQmUE03UTgzdUduUGN1RHNZMmxOTWlwV2NESjJKbkpQVjBWRVVpUW1VRTAzVVRnemRVZHVVR04xUkhNPQ=="));

    for (auto item : pairs)
    {
        std::string encoded = base64_encode(item.first);
		EXPECT_EQ(encoded, item.second);
		std::string decoded = base64_decode(encoded);
		EXPECT_EQ(decoded, item.first);
    }
    
}
