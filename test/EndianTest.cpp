#include "Utility/Endian.h"
#include <gtest/gtest.h>

using namespace std;

TEST(Endian, ByteSwap)
{
    std::vector<std::pair<int64_t, int64_t>> pairs;
    pairs.push_back(std::make_pair(0x1234567898765432, 0x3254769878563412));

    for (auto item : pairs)
    {
        int64_t value = XNET_SWAP64(item.first);
        EXPECT_EQ(value, item.second);
    }
}

TEST(Endian, LittleEndian) 
{
    char buf[1024];
    LittleEndian::PutUint16(buf, 0x12);
    auto v = BigEndian::GetUint16(buf);
    EXPECT_EQ(v, 0x12);
}

TEST(Endian, BigEndian)
{

}